-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 24, 2020 at 11:29 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `maganic`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `description` text NOT NULL,
  `image` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`description`, `image`, `created_at`, `updated_at`, `id`, `lang_id`) VALUES
('u6yhtrge', '/uploads/files/Capture.png', '2020-06-23 18:40:37', '2020-06-23 18:47:54', 7, 0),
('hthtrhtherhtheht', '/uploads/files/Capture_1.png', '2020-06-24 20:38:37', '2020-06-24 20:38:45', 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `auther`
--

CREATE TABLE `auther` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auther`
--

INSERT INTO `auther` (`id`, `name`, `description`) VALUES
(1, 'hisham', 'grgrgregrgrgregergregregregegrgrgergregreg'),
(2, 'bassem', '0gegergergrgregregregergergergergrgrgrgergregerger');

-- --------------------------------------------------------

--
-- Table structure for table `book`
--

CREATE TABLE `book` (
  `id` int(11) NOT NULL,
  `name_ar` varchar(25) NOT NULL,
  `name_en` varchar(25) NOT NULL,
  `date_of_publication` date NOT NULL,
  `ISBN` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `Edition` int(11) NOT NULL,
  `original_publisher` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `number_page` int(11) NOT NULL,
  `dimensions` int(11) NOT NULL,
  `Available_versions` int(11) NOT NULL,
  `ISBN_ELECTRONIC` int(11) NOT NULL,
  `ISBN_PRINTED` int(11) NOT NULL,
  `symbol_book` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `currency_id` int(11) NOT NULL,
  `about_book` varchar(100) NOT NULL,
  `about_auther` varchar(100) NOT NULL,
  `about_translator` varchar(100) NOT NULL,
  `image_book` varchar(100) NOT NULL,
  `image_auther` varchar(100) NOT NULL,
  `image_translator` varchar(100) NOT NULL,
  `evaluation` int(11) NOT NULL,
  `discount_price` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `book`
--

INSERT INTO `book` (`id`, `name_ar`, `name_en`, `date_of_publication`, `ISBN`, `lang_id`, `Edition`, `original_publisher`, `cat_id`, `number_page`, `dimensions`, `Available_versions`, `ISBN_ELECTRONIC`, `ISBN_PRINTED`, `symbol_book`, `price`, `currency_id`, `about_book`, `about_auther`, `about_translator`, `image_book`, `image_auther`, `image_translator`, `evaluation`, `discount_price`, `updated_at`, `created_at`) VALUES
(40, 'كتاب 1', '', '0000-00-00', 0, 1, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 1, '', '', '', '1592855442.jpg', 'E:\\xampp\\tmp\\php9EAF.tmp', 'E:\\xampp\\tmp\\php9EB0.tmp', 0, 0, '2020-06-22 19:50:42', '2020-06-22 19:50:42'),
(41, 'كتاب 1', '', '0000-00-00', 0, 1, 0, 1, 2, 0, 0, 0, 0, 0, 0, 0, 1, '', '', '', 'hisham.jpg', 'hisham.jpg', 'hisham.jpg', 0, 0, '2020-06-24 20:32:43', '2020-06-22 19:50:57');

-- --------------------------------------------------------

--
-- Table structure for table `book_auther`
--

CREATE TABLE `book_auther` (
  `auther_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `book_auther`
--

INSERT INTO `book_auther` (`auther_id`, `book_id`, `updated_at`, `created_at`, `id`) VALUES
(2, 2, '2020-06-19 12:08:11', '2020-06-19 12:08:11', 3),
(1, 2, '2020-06-19 12:08:46', '2020-06-19 12:08:46', 4),
(2, 2, '2020-06-19 12:08:46', '2020-06-19 12:08:46', 5),
(1, 2, '2020-06-19 12:14:41', '2020-06-19 12:14:41', 6),
(2, 2, '2020-06-19 12:14:41', '2020-06-19 12:14:41', 7),
(1, 2, '2020-06-19 12:15:12', '2020-06-19 12:15:12', 8),
(2, 2, '2020-06-19 12:15:12', '2020-06-19 12:15:12', 9),
(1, 2, '2020-06-19 12:15:33', '2020-06-19 12:15:33', 10),
(2, 2, '2020-06-19 12:15:33', '2020-06-19 12:15:33', 11),
(1, 2, '2020-06-19 12:15:49', '2020-06-19 12:15:49', 12),
(2, 2, '2020-06-19 12:15:49', '2020-06-19 12:15:49', 13),
(1, 2, '2020-06-22 19:07:27', '2020-06-22 19:07:27', 14),
(1, 2, '2020-06-22 19:11:51', '2020-06-22 19:11:51', 15),
(1, 2, '2020-06-22 19:18:13', '2020-06-22 19:18:13', 16),
(1, 2, '2020-06-22 19:22:08', '2020-06-22 19:22:08', 17),
(2, 2, '2020-06-22 19:25:22', '2020-06-22 19:25:22', 18),
(1, 2, '2020-06-22 19:26:05', '2020-06-22 19:26:05', 19),
(2, 2, '2020-06-22 19:29:45', '2020-06-22 19:29:45', 20),
(2, 2, '2020-06-22 19:30:33', '2020-06-22 19:30:33', 21),
(1, 2, '2020-06-22 19:34:03', '2020-06-22 19:34:03', 22),
(2, 2, '2020-06-22 19:42:37', '2020-06-22 19:42:37', 23),
(1, 2, '2020-06-22 19:44:38', '2020-06-22 19:44:38', 24),
(1, 2, '2020-06-22 19:48:40', '2020-06-22 19:48:40', 25);

-- --------------------------------------------------------

--
-- Table structure for table `book_translator`
--

CREATE TABLE `book_translator` (
  `translator_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `book_translator`
--

INSERT INTO `book_translator` (`translator_id`, `book_id`, `updated_at`, `created_at`, `id`) VALUES
(1, 2, '2020-06-19 12:15:49', '2020-06-19 12:15:49', 1),
(2, 2, '2020-06-19 12:15:49', '2020-06-19 12:15:49', 2),
(1, 2, '2020-06-22 19:07:28', '2020-06-22 19:07:28', 3),
(2, 2, '2020-06-22 19:11:51', '2020-06-22 19:11:51', 4),
(1, 2, '2020-06-22 19:18:13', '2020-06-22 19:18:13', 5),
(1, 2, '2020-06-22 19:22:08', '2020-06-22 19:22:08', 6),
(1, 2, '2020-06-22 19:25:22', '2020-06-22 19:25:22', 7),
(1, 2, '2020-06-22 19:26:05', '2020-06-22 19:26:05', 8),
(1, 2, '2020-06-22 19:29:45', '2020-06-22 19:29:45', 9),
(2, 2, '2020-06-22 19:30:33', '2020-06-22 19:30:33', 10),
(1, 2, '2020-06-22 19:34:03', '2020-06-22 19:34:03', 11),
(1, 2, '2020-06-22 19:42:37', '2020-06-22 19:42:37', 12),
(1, 2, '2020-06-22 19:44:39', '2020-06-22 19:44:39', 13),
(1, 2, '2020-06-22 19:48:40', '2020-06-22 19:48:40', 14);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `parent_cat` int(11) DEFAULT NULL,
  `depth` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `url` varchar(200) DEFAULT NULL,
  `brief` varchar(200) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `status` tinyint(2) DEFAULT '1',
  `sort_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `parent_cat`, `depth`, `name`, `url`, `brief`, `image`, `description`, `status`, `sort_order`, `created_at`, `updated_at`) VALUES
(2, 0, 0, 'cat111', 'cat1', 'cat 1', '/uploads/files/knife.jpg', '<p>cat desc</p>', 0, 1, '2017-06-12 11:04:23', '2017-07-05 09:00:54'),
(3, 2, 1, 'son1', 'son1', 'son1', '/uploads/files/Capture.png', 'desccc', 0, 1, '2017-06-12 11:14:04', '2017-07-05 06:47:30'),
(4, 0, 0, 'cat3', '', 'cat3', '/uploads/files/Capture.png', '', 1, 1, '2017-06-12 12:01:20', '2017-06-12 12:01:20'),
(5, 3, 2, 'sonOfSon', 'htttp://www.sonofson.com', 'this is a third depth son', '/uploads/files/Capture.png', '<p>no desc</p>', 1, 1, '2017-07-05 06:07:03', '2017-07-05 09:03:20');

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `phone` int(11) NOT NULL,
  `fax` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `facebook` text NOT NULL,
  `inst` text NOT NULL,
  `youtube` text NOT NULL,
  `telegram` text NOT NULL,
  `twiter` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `phone`, `fax`, `email`, `address`, `facebook`, `inst`, `youtube`, `telegram`, `twiter`) VALUES
(1, 6802446, 6802446, 'moawenhisham', 'france - baris', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `countries_list`
--

CREATE TABLE `countries_list` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `iso2` varchar(2) NOT NULL DEFAULT '',
  `iso3` varchar(3) NOT NULL DEFAULT '',
  `ar_name` varchar(255) NOT NULL,
  `fr_name` varchar(255) NOT NULL,
  `currency` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `countries_list`
--

INSERT INTO `countries_list` (`id`, `name`, `iso2`, `iso3`, `ar_name`, `fr_name`, `currency`) VALUES
(1, 'Afghanistan', 'AF', 'AFG', 'أفغانستان', 'Afghanistan', 'AFN'),
(2, 'Albania', 'AL', 'ALB', 'ألبانيا', 'Albanie', 'ALL'),
(3, 'Algeria', 'DZ', 'DZA', 'الجزائر', 'Algérie', 'DZD'),
(4, 'American Samoa', 'AS', 'ASM', 'ساموا الأميركية', 'Samoa américaine', 'USD'),
(5, 'Andorra', 'AD', 'AND', 'اندورا', 'Andorre', 'EUR'),
(6, 'Angola', 'AO', 'AGO', 'انغولا', 'Angola', 'AOA'),
(7, 'Anguilla', 'AI', 'AIA', 'انغوليا', 'Anguilla', 'XCD'),
(8, 'Antarctica', 'AQ', 'ATA', 'انتارتيكا', 'Antarctique', ''),
(9, 'Antigua and Barbuda', 'AG', 'ATG', 'أنتيغوا و باربودا', 'Antigua-et-Barbuda', 'XCD'),
(10, 'Argentina', 'AR', 'ARG', 'الأرجنتين', 'Argentine', 'ARS'),
(11, 'Armenia', 'AM', 'ARM', 'أرمينيا', 'Arménie', 'AMD'),
(12, 'Aruba', 'AW', 'ABW', 'اروبا', 'Aruba', 'AWG'),
(13, 'Australia', 'AU', 'AUS', 'استراليا', 'Australie', 'AUD'),
(14, 'Austria', 'AT', 'AUT', 'النمسا', 'Autriche', 'EUR'),
(15, 'Azerbaijan', 'AZ', 'AZE', 'أذربيجان', 'Azerbaïdjan', 'AZN'),
(16, 'Bahamas', 'BS', 'BHS', 'البهاماس', 'Bahamas', 'BSD'),
(17, 'Bahrain', 'BH', 'BHR', 'البحرين', 'Bahreïn', 'BHD'),
(18, 'Bangladesh', 'BD', 'BGD', 'بنغلاديش', 'Bangladesh', 'BDT'),
(19, 'Barbados', 'BB', 'BRB', 'باربادوس', 'Barbade', 'BBD'),
(20, 'Belarus', 'BY', 'BLR', 'بيلاروس', 'Bélarus', 'BYR'),
(21, 'Belgium', 'BE', 'BEL', 'بلجيكا', 'Belgique', 'EUR'),
(22, 'Belize', 'BZ', 'BLZ', 'بليز', 'Belize', 'BZD'),
(23, 'Benin', 'BJ', 'BEN', 'بنين', 'Bénin', 'XOF'),
(24, 'Bermuda', 'BM', 'BMU', 'برمودا', 'Bermudes', 'BMD'),
(25, 'Bhutan', 'BT', 'BTN', 'بوتان', 'Bhoutan', 'BTN'),
(26, 'Bolivia', 'BO', 'BOL', 'بوليفيا', 'Bolivie', 'BOB'),
(27, 'Bosnia and Herzegowina', 'BA', 'BIH', 'البوسنة و الهرسك', 'Bosnie-Herzégovine', 'BAM'),
(28, 'Botswana', 'BW', 'BWA', 'بوتسوانا', 'Botswana', 'BWP'),
(29, 'Bouvet Island', 'BV', 'BVT', 'جزيرة بوفيه', 'Île Bouvet', 'NOK'),
(30, 'Brazil', 'BR', 'BRA', 'البرازيل', 'Brésil', 'BRL'),
(31, 'British Indian Ocean Territory', 'IO', 'IOT', 'إقليم المحيط الهندي البريطاني', 'Territoire britannique de l\'océan Indien', 'USD'),
(32, 'Brunei Darussalam', 'BN', 'BRN', 'بروناي', 'Brunei Darussalam', 'BND'),
(33, 'Bulgaria', 'BG', 'BGR', 'بلغاريا', 'Bulgarie', 'BGN'),
(34, 'Burkina Faso', 'BF', 'BFA', 'بوركينا فاسو', 'Burkina Faso', 'XOF'),
(35, 'Burundi', 'BI', 'BDI', 'بوروندي', 'Burundi', 'BIF'),
(36, 'Cambodia', 'KH', 'KHM', 'كامبوديا', 'Cambodge', 'KHR'),
(37, 'Cameroon', 'CM', 'CMR', 'الكاميرون', 'Cameroun', 'XAF'),
(38, 'Canada', 'CA', 'CAN', 'كندا', 'Canada', 'CAD'),
(39, 'Cape Verde', 'CV', 'CPV', 'الرأس الأخضر', 'Cap-Vert', 'CVE'),
(40, 'Cayman Islands', 'KY', 'CYM', 'جزر كايمان', 'Îles Caïmans', 'KYD'),
(41, 'Central African Republic', 'CF', 'CAF', 'جمهورية افريقية الوسطى', 'République centrafricaine', 'XAF'),
(42, 'Chad', 'TD', 'TCD', 'التشاد', 'Tchad', 'XAF'),
(43, 'Chile', 'CL', 'CHL', 'التشيلي', 'Chili', 'CLP'),
(44, 'China', 'CN', 'CHN', 'الصين', 'Chine', 'CNY'),
(45, 'Christmas Island', 'CX', 'CXR', 'جزر عيد الميلاد', 'Île Christmas', 'AUD'),
(46, 'Cocos (Keeling) Islands', 'CC', 'CCK', 'جزر كوكس', 'Îles Cocos (Keeling)', 'AUD'),
(47, 'Colombia', 'CO', 'COL', 'كولومبيا', 'Colombie', 'COP'),
(48, 'Comoros', 'KM', 'COM', 'جزر القمر', 'Comores', 'KMF'),
(49, 'Congo', 'CG', 'COG', 'الكانغو', 'Congo', 'XAF'),
(50, 'Cook Islands', 'CK', 'COK', '', 'Îles Cook', 'NZD'),
(51, 'Costa Rica', 'CR', 'CRI', 'كوستا ريكا', 'Costa Rica', 'CRC'),
(52, 'Cote D\'Ivoire', 'CI', 'CIV', 'ساحل العاج', 'Côte d\'Ivoire', 'XOF'),
(53, 'Croatia', 'HR', 'HRV', 'كرواتيا', 'Croatie', 'HRK'),
(54, 'Cuba', 'CU', 'CUB', 'كوبا', 'Cuba', 'CUP'),
(55, 'Cyprus', 'CY', 'CYP', 'قبرص', 'Chypre', 'EUR'),
(56, 'Czech Republic', 'CZ', 'CZE', 'جمهورية التشيك', 'République tchèque', 'CZK'),
(57, 'Denmark', 'DK', 'DNK', 'الدنمارك', 'Danemark', 'DKK'),
(58, 'Djibouti', 'DJ', 'DJI', '', 'Djibouti', 'DJF'),
(59, 'Dominica', 'DM', 'DMA', '', 'Dominique', 'XCD'),
(60, 'Dominican Republic', 'DO', 'DOM', '', 'République dominicaine', 'DOP'),
(61, 'East Timor', 'TP', 'TMP', 'تيمور الشرقية', '', NULL),
(62, 'Ecuador', 'EC', 'ECU', 'اكوادور', 'Équateur', 'USD'),
(63, 'Egypt', 'EG', 'EGY', 'مصر', 'Égypte', 'EGP'),
(64, 'El Salvador', 'SV', 'SLV', 'السلفادور', 'El Salvador', 'USD'),
(65, 'Equatorial Guinea', 'GQ', 'GNQ', 'غينيا الاستوائية', 'Guinée équatoriale', 'XAF'),
(66, 'Eritrea', 'ER', 'ERI', 'ارتيريا', 'Érythrée', 'ERN'),
(67, 'Estonia', 'EE', 'EST', 'استونيا', 'Estonie', 'EUR'),
(68, 'Ethiopia', 'ET', 'ETH', 'اثيوبيا', 'Éthiopie', 'ETB'),
(69, 'Falkland Islands (Malvinas)', 'FK', 'FLK', 'جزر فوكلاند', 'Îles Malouines', 'FKP'),
(70, 'Faroe Islands', 'FO', 'FRO', 'جزر فارو', 'Îles Féroé', 'DKK'),
(71, 'Fiji', 'FJ', 'FJI', 'فيجي', 'Fidji', 'FJD'),
(72, 'Finland', 'FI', 'FIN', 'فنلندا', 'Finlande', 'EUR'),
(73, 'France', 'FR', 'FRA', 'فرنسا', 'France', 'EUR'),
(74, 'France, Metropolitan', 'FX', 'FXX', '', '', NULL),
(75, 'French Guiana', 'GF', 'GUF', 'غويانا الفرنسية', 'Guyane française', 'EUR'),
(76, 'French Polynesia', 'PF', 'PYF', 'بولينزيا الفرنسية', 'Polynésie française', 'XPF'),
(77, 'French Southern Territories', 'TF', 'ATF', 'أراضٍ فرنسية جنوبية وأنتارتيكية', 'Terres australes françaises', 'EUR'),
(78, 'Gabon', 'GA', 'GAB', 'الغابون', 'Gabon', 'XAF'),
(79, 'Gambia', 'GM', 'GMB', 'غامبيا', 'Gambie', 'GMD'),
(80, 'Georgia', 'GE', 'GEO', 'جورجيا', 'Géorgie', 'GEL'),
(81, 'Germany', 'DE', 'DEU', 'المانيا', 'Allemagne', 'EUR'),
(82, 'Ghana', 'GH', 'GHA', 'غانا', 'Ghana', 'GHS'),
(83, 'Gibraltar', 'GI', 'GIB', 'جبل طارق', 'Gibraltar', 'GIP'),
(84, 'Greece', 'GR', 'GRC', 'اليونان', 'Grèce', 'EUR'),
(85, 'Greenland', 'GL', 'GRL', 'جرينلاند', 'Groenland', 'DKK'),
(86, 'Grenada', 'GD', 'GRD', 'جرينادا', 'Grenade', 'XCD'),
(87, 'Guadeloupe', 'GP', 'GLP', 'جوادلوب', 'Guadeloupe', 'EUR'),
(88, 'Guam', 'GU', 'GUM', 'غوام', 'Guam', 'USD'),
(89, 'Guatemala', 'GT', 'GTM', 'غواتيمالا', 'Guatemala', 'GTQ'),
(90, 'Guinea', 'GN', 'GIN', 'غينيا', 'Guinée', 'GNF'),
(91, 'Guinea-bissau', 'GW', 'GNB', 'غينيا بيساو', 'Guinée-Bissau', 'XOF'),
(92, 'Guyana', 'GY', 'GUY', 'غويانا', 'Guyana', 'GYD'),
(93, 'Haiti', 'HT', 'HTI', 'هايتي', 'Haïti', 'HTG'),
(94, 'Heard and Mc Donald Islands', 'HM', 'HMD', 'جزيرة هيرد وجزر ماكدونالد', 'Îles Heard et McDonald', 'AUD'),
(95, 'Honduras', 'HN', 'HND', 'هندوراس', 'Honduras', 'HNL'),
(96, 'Hong Kong', 'HK', 'HKG', 'هونغ كونغ', 'Hong Kong', 'HKD'),
(97, 'Hungary', 'HU', 'HUN', 'هنغاريا', 'Hongrie', 'HUF'),
(98, 'Iceland', 'IS', 'ISL', 'ايسلندا', 'Islande', 'ISK'),
(99, 'India', 'IN', 'IND', 'الهند', 'Inde', 'INR'),
(100, 'Indonesia', 'ID', 'IDN', 'اندونيسيا', 'Indonésie', 'IDR'),
(101, 'Iran (Islamic Republic of)', 'IR', 'IRN', 'ايران', 'Iran', 'IRR'),
(102, 'Iraq', 'IQ', 'IRQ', 'العراق', 'Irak', 'IQD'),
(103, 'Ireland', 'IE', 'IRL', 'ايرلندا', 'Irlande', 'EUR'),
(104, 'Israel', 'IL', 'ISR', '', 'Israël', 'ILS'),
(105, 'Italy', 'IT', 'ITA', 'ايطاليا', 'Italie', 'EUR'),
(106, 'Jamaica', 'JM', 'JAM', 'جامايكا', 'Jamaïque', 'JMD'),
(107, 'Japan', 'JP', 'JPN', 'اليابان', 'Japon', 'JPY'),
(108, 'Jordan', 'JO', 'JOR', 'الأردن', 'Jordanie', 'JOD'),
(109, 'Kazakhstan', 'KZ', 'KAZ', 'كازاخستان', 'Kazakhstan', 'KZT'),
(110, 'Kenya', 'KE', 'KEN', 'كينيا', 'Kenya', 'KES'),
(111, 'Kiribati', 'KI', 'KIR', 'كيريباتي', 'Kiribati', 'AUD'),
(112, 'Korea, Democratic People\'s Republic of', 'KP', 'PRK', 'كوريا الشمالية', 'Corée du Nord', 'KPW'),
(113, 'Korea, Republic of', 'KR', 'KOR', 'كوريا الجنوبية', 'Corée du Sud', 'KRW'),
(114, 'Kuwait', 'KW', 'KWT', 'الكويت', 'Koweït', 'KWD'),
(115, 'Kyrgyzstan', 'KG', 'KGZ', 'قرغيستان', 'Kirghizistan', 'KGS'),
(116, 'Lao People\'s Democratic Republic', 'LA', 'LAO', 'لاوس', 'Laos', 'LAK'),
(117, 'Latvia', 'LV', 'LVA', 'لاتفيا', 'Lettonie', 'EUR'),
(118, 'Lebanon', 'LB', 'LBN', 'لبنان', 'Liban', 'LBP'),
(119, 'Lesotho', 'LS', 'LSO', 'ليسوتو', 'Lesotho', 'LSL'),
(120, 'Liberia', 'LR', 'LBR', 'ليبيريا', 'Libéria', 'LRD'),
(121, 'Libyan Arab Jamahiriya', 'LY', 'LBY', 'ليبيا', 'Libye', 'LYD'),
(122, 'Liechtenstein', 'LI', 'LIE', 'ليختنشتاين', 'Liechtenstein', 'CHF'),
(123, 'Lithuania', 'LT', 'LTU', 'ليتوانيا', 'Lituanie', 'EUR'),
(124, 'Luxembourg', 'LU', 'LUX', 'لوكسمبورغ', 'Luxembourg', 'EUR'),
(125, 'Macau', 'MO', 'MAC', 'ماكاو', 'Macao', 'MOP'),
(126, 'Macedonia, The Former Yugoslav Republic of', 'MK', 'MKD', 'مقدونيا', 'Macédoine', 'MKD'),
(127, 'Madagascar', 'MG', 'MDG', 'مدغشقر', 'Madagascar', 'MGA'),
(128, 'Malawi', 'MW', 'MWI', 'مالاوي', 'Malawi', 'MWK'),
(129, 'Malaysia', 'MY', 'MYS', 'ماليزيا', 'Malaisie', 'MYR'),
(130, 'Maldives', 'MV', 'MDV', 'مالديف', 'Maldives', 'MVR'),
(131, 'Mali', 'ML', 'MLI', 'مالي', 'Mali', 'XOF'),
(132, 'Malta', 'MT', 'MLT', 'مالطا', 'Malte', 'EUR'),
(133, 'Marshall Islands', 'MH', 'MHL', 'جزر المارشال', 'Îles Marshall', 'USD'),
(134, 'Martinique', 'MQ', 'MTQ', 'مارتينيك', 'Martinique', 'EUR'),
(135, 'Mauritania', 'MR', 'MRT', 'موريتانيا', 'Mauritanie', 'MRO'),
(136, 'Mauritius', 'MU', 'MUS', 'موريشيوس', 'Maurice', 'MUR'),
(137, 'Mayotte', 'YT', 'MYT', 'مايوت', 'Mayotte', 'EUR'),
(138, 'Mexico', 'MX', 'MEX', 'المكسيك', 'Mexique', 'MXN'),
(139, 'Micronesia, Federated States of', 'FM', 'FSM', 'ولايات ميكرونيسيا المتحدة', 'Micronésie, États fédérés de', 'USD'),
(140, 'Moldova, Republic of', 'MD', 'MDA', 'مولدوفا', 'Moldavie', 'MDL'),
(141, 'Monaco', 'MC', 'MCO', 'موناكو', 'Monaco', 'EUR'),
(142, 'Mongolia', 'MN', 'MNG', 'منغوليا', 'Mongolie', 'MNT'),
(143, 'Montserrat', 'MS', 'MSR', 'مونتسرات', 'Montserrat', 'XCD'),
(144, 'Morocco', 'MA', 'MAR', 'المغرب', 'Maroc', 'MAD'),
(145, 'Mozambique', 'MZ', 'MOZ', 'الموزمبيق', 'Mozambique', 'MZN'),
(146, 'Myanmar', 'MM', 'MMR', 'بورما', 'Myanmar', 'MMK'),
(147, 'Namibia', 'NA', 'NAM', 'ناميبيا', 'Namibie', 'NAD'),
(148, 'Nauru', 'NR', 'NRU', 'ناورو', 'Nauru', 'AUD'),
(149, 'Nepal', 'NP', 'NPL', 'النيبال', 'Népal', 'NPR'),
(150, 'Netherlands', 'NL', 'NLD', 'هولندا', 'Pays-Bas', 'EUR'),
(151, 'Netherlands Antilles', 'AN', 'ANT', 'جزر الأنتيل الهولندية', 'Netherlands Antilles', NULL),
(152, 'New Caledonia', 'NC', 'NCL', 'كاليدونيا الجديدة', 'Nouvelle-Calédonie', 'XPF'),
(153, 'New Zealand', 'NZ', 'NZL', 'نيوزيلندا', 'Nouvelle-Zélande', 'NZD'),
(154, 'Nicaragua', 'NI', 'NIC', 'نيكاراجوا', 'Nicaragua', 'NIO'),
(155, 'Niger', 'NE', 'NER', 'النيجر', 'Niger', 'XOF'),
(156, 'Nigeria', 'NG', 'NGA', 'نيجيريا', 'Nigeria', 'NGN'),
(157, 'Niue', 'NU', 'NIU', 'نييوي', 'Niue', 'NZD'),
(158, 'Norfolk Island', 'NF', 'NFK', '', 'Île Norfolk', 'AUD'),
(159, 'Northern Mariana Islands', 'MP', 'MNP', '', 'Mariannes du Nord', 'USD'),
(160, 'Norway', 'NO', 'NOR', 'النروج', 'Norvège', 'NOK'),
(161, 'Oman', 'OM', 'OMN', 'سلطنة عمان', 'Oman', 'OMR'),
(162, 'Pakistan', 'PK', 'PAK', 'باكستان', 'Pakistan', 'PKR'),
(163, 'Palau', 'PW', 'PLW', '', 'Palau', 'USD'),
(164, 'Panama', 'PA', 'PAN', 'باناما', 'Panama', 'PAB'),
(165, 'Papua New Guinea', 'PG', 'PNG', '', 'Papouasie-Nouvelle-Guinée', 'PGK'),
(166, 'Paraguay', 'PY', 'PRY', 'البروغوي', 'Paraguay', 'PYG'),
(167, 'Peru', 'PE', 'PER', 'اليبرو', 'Pérou', 'PEN'),
(168, 'Philippines', 'PH', 'PHL', 'الفلبين', 'Philippines', 'PHP'),
(169, 'Pitcairn', 'PN', 'PCN', '', 'Pitcairn', 'NZD'),
(170, 'Poland', 'PL', 'POL', 'بولندا', 'Pologne', 'PLN'),
(171, 'Portugal', 'PT', 'PRT', 'البرتغال', 'Portugal', 'EUR'),
(172, 'Puerto Rico', 'PR', 'PRI', 'بورتوريكو', 'Puerto Rico', 'USD'),
(173, 'Qatar', 'QA', 'QAT', 'قطر', 'Qatar', 'QAR'),
(174, 'Reunion', 'RE', 'REU', '', 'Réunion', 'EUR'),
(175, 'Romania', 'RO', 'ROM', 'رومانيا', 'Roumanie', NULL),
(176, 'Russian Federation', 'RU', 'RUS', 'روسيا', 'Russie', 'RUB'),
(177, 'Rwanda', 'RW', 'RWA', '', 'Rwanda', 'RWF'),
(178, 'Saint Kitts and Nevis', 'KN', 'KNA', '', 'Saint-Kitts-et-Nevis', 'XCD'),
(179, 'Saint Lucia', 'LC', 'LCA', '', 'Sainte-Lucie', 'XCD'),
(180, 'Saint Vincent and the Grenadines', 'VC', 'VCT', '', 'Saint-Vincent-et-les-Grenadines', 'XCD'),
(181, 'Samoa', 'WS', 'WSM', 'ساموا', 'Samoa', 'WST'),
(182, 'San Marino', 'SM', 'SMR', '', 'Saint-Marin', 'EUR'),
(183, 'Sao Tome and Principe', 'ST', 'STP', '', 'Sao Tomé-et-Principe', 'STD'),
(184, 'Saudi Arabia', 'SA', 'SAU', 'السعودية', 'Arabie saoudite', 'SAR'),
(185, 'Senegal', 'SN', 'SEN', 'السنغال', 'Sénégal', 'XOF'),
(186, 'Seychelles', 'SC', 'SYC', '', 'Seychelles', 'SCR'),
(187, 'Sierra Leone', 'SL', 'SLE', '', 'Sierra Leone', 'SLL'),
(188, 'Singapore', 'SG', 'SGP', 'سنغافورة', 'Singapour', 'SGD'),
(189, 'Slovakia (Slovak Republic)', 'SK', 'SVK', 'سلوفاكيا', 'Slovaquie', 'EUR'),
(190, 'Slovenia', 'SI', 'SVN', 'سلوفينيا', 'Slovénie', 'EUR'),
(191, 'Solomon Islands', 'SB', 'SLB', '', 'Îles Salomon', 'SBD'),
(192, 'Somalia', 'SO', 'SOM', '', 'Somalie', 'SOS'),
(193, 'South Africa', 'ZA', 'ZAF', 'جنوب افريقيا', 'Afrique du Sud', 'ZAR'),
(194, 'South Georgia and the South Sandwich Islands', 'GS', 'SGS', '', 'Géorgie du Sud et les îles Sandwich du Sud', 'GBP'),
(195, 'Spain', 'ES', 'ESP', 'اسبانيا', 'Espagne', 'EUR'),
(196, 'Sri Lanka', 'LK', 'LKA', '', 'Sri Lanka', 'LKR'),
(197, 'St. Helena', 'SH', 'SHN', '', 'Sainte-Hélène', 'SHP'),
(198, 'St. Pierre and Miquelon', 'PM', 'SPM', '', 'Saint-Pierre-et-Miquelon', 'EUR'),
(199, 'Sudan', 'SD', 'SDN', 'السودان', 'Soudan', 'SDG'),
(200, 'Suriname', 'SR', 'SUR', '', 'Suriname', 'SRD'),
(201, 'Svalbard and Jan Mayen Islands', 'SJ', 'SJM', '', 'Svalbard et île de Jan Mayen', 'NOK'),
(202, 'Swaziland', 'SZ', 'SWZ', '', 'Swaziland', 'SZL'),
(203, 'Sweden', 'SE', 'SWE', 'السويد', 'Suède', 'SEK'),
(204, 'Switzerland', 'CH', 'CHE', 'سويسرا', 'Suisse', 'CHF'),
(205, 'Syrian Arab Republic', 'SY', 'SYR', 'سوريا', 'Syrie', 'SYP'),
(206, 'Taiwan', 'TW', 'TWN', 'تايون', 'Taïwan', 'TWD'),
(207, 'Tajikistan', 'TJ', 'TJK', 'طاجيكستان', 'Tadjikistan', 'TJS'),
(208, 'Tanzania, United Republic of', 'TZ', 'TZA', 'تانزانيا', 'Tanzanie', 'TZS'),
(209, 'Thailand', 'TH', 'THA', 'تايلاند', 'Thaïlande', 'THB'),
(210, 'Togo', 'TG', 'TGO', 'توغو', 'Togo', 'XOF'),
(211, 'Tokelau', 'TK', 'TKL', '', 'Tokelau', 'NZD'),
(212, 'Tonga', 'TO', 'TON', '', 'Tonga', 'TOP'),
(213, 'Trinidad and Tobago', 'TT', 'TTO', '', 'Trinité-et-Tobago', 'TTD'),
(214, 'Tunisia', 'TN', 'TUN', 'تونس', 'Tunisie', 'TND'),
(215, 'Turkey', 'TR', 'TUR', 'تركيا', 'Turquie', 'TRY'),
(216, 'Turkmenistan', 'TM', 'TKM', 'تركمنستان', 'Turkménistan', 'TMT'),
(217, 'Turks and Caicos Islands', 'TC', 'TCA', '', 'Îles Turks et Caicos', 'USD'),
(218, 'Tuvalu', 'TV', 'TUV', '', 'Tuvalu', 'AUD'),
(219, 'Uganda', 'UG', 'UGA', 'أوغندا', 'Ouganda', 'UGX'),
(220, 'Ukraine', 'UA', 'UKR', 'أوكرانيا', 'Ukraine', 'UAH'),
(221, 'United Arab Emirates', 'AE', 'ARE', 'الإمارات العريبة المتحدة', 'Émirats arabes unis', 'AED'),
(222, 'United Kingdom', 'GB', 'GBR', 'بريطانيا', 'Royaume-Uni', 'GBP'),
(223, 'United States', 'US', 'USA', 'اميركا', 'États-Unis', 'USD'),
(224, 'United States Minor Outlying Islands', 'UM', 'UMI', '', 'Îles mineures éloignées des États-Unis', 'USD'),
(225, 'Uruguay', 'UY', 'URY', 'اورغوي', 'Uruguay', 'UYU'),
(226, 'Uzbekistan', 'UZ', 'UZB', 'أوزباكستان', 'Ouzbékistan', 'UZS'),
(227, 'Vanuatu', 'VU', 'VUT', '', 'Vanuatu', 'VUV'),
(228, 'Vatican City State (Holy See)', 'VA', 'VAT', '', 'Vatican', 'EUR'),
(229, 'Venezuela', 'VE', 'VEN', '', 'Venezuela', 'VEF'),
(230, 'Viet Nam', 'VN', 'VNM', 'فيتنام', 'Vietnam', 'VND'),
(231, 'Virgin Islands (British)', 'VG', 'VGB', '', 'Îles Vierges britanniques', 'USD'),
(232, 'Virgin Islands (U.S.)', 'VI', 'VIR', '', 'Îles Vierges américaines', 'USD'),
(233, 'Wallis and Futuna Islands', 'WF', 'WLF', '', 'Îles Wallis-et-Futuna', 'XPF'),
(234, 'Western Sahara', 'EH', 'ESH', '', 'Sahara Occidental', 'MAD'),
(235, 'Yemen', 'YE', 'YEM', 'اليمن', 'Yémen', 'YER'),
(236, 'Yugoslavia', 'YU', 'YUG', 'يوغسلافيا', 'Yugoslavia', NULL),
(237, 'Zaire', 'ZR', 'ZAR', '', 'Zaire', NULL),
(238, 'Zambia', 'ZM', 'ZMB', 'زامبيا', 'Zambie', 'ZMW'),
(239, 'Zimbabwe', 'ZW', 'ZWE', 'زيمبابوي', 'Zimbabwe', 'ZWL');

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `name`) VALUES
(1, 'dolar'),
(2, 'euro');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `name`) VALUES
(1, 'السياسية'),
(2, 'اقتصادية');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name`, `updated_at`, `created_at`, `lang_id`) VALUES
(10, 'القيم', '2020-06-24 00:00:00', '2020-06-24 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image_url` varchar(100) NOT NULL,
  `title` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `name` varchar(25) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`name`, `id`) VALUES
('العربية', 1),
('الانكليزية', 2);

-- --------------------------------------------------------

--
-- Table structure for table `lectures`
--

CREATE TABLE `lectures` (
  `id` int(11) NOT NULL,
  `image` varchar(25) NOT NULL,
  `main_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `professor` varchar(25) NOT NULL,
  `title` varchar(25) NOT NULL,
  `date` date NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` int(11) NOT NULL,
  `vedio` text,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lectures`
--

INSERT INTO `lectures` (`id`, `image`, `main_id`, `branch_id`, `professor`, `title`, `date`, `updated_at`, `created_at`, `vedio`, `lang_id`) VALUES
(14, '/uploads/files/Capture.pn', 10, 1, '3', 'first typeuu6uuu', '2020-06-10', '2020-06-24 19:32:41', 2020, NULL, 1),
(15, '/uploads/files/Capture.pn', 10, 2, '3', 'first kuykyukyukuyk', '2020-06-26', '2020-06-24 20:29:01', 2020, NULL, 1),
(16, '', 10, 1, '3', 'slider', '2020-06-13', '2020-06-24 20:28:55', 2020, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_type` varchar(50) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_type` varchar(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  `old_value` text NOT NULL,
  `new_value` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `user_id`, `user_type`, `item_id`, `item_type`, `message`, `old_value`, `new_value`, `created_at`, `updated_at`) VALUES
(1, 1, 'system', 4, 'Prodcuts', 'Add new product (hisham)', '', '{\"cat_id\":\"2\",\"name\":\"hisham\",\"description\":\"<p><strong>rgregregergr<\\/strong><\\/p>\",\"brief\":\"etqg\",\"image\":\"\\/uploads\\/files\\/Capture_1.png\",\"url\":\"rgqrg\",\"status\":\"1\",\"sort_order\":\"1\",\"updated_at\":\"2020-06-02 19:07:48\",\"created_at\":\"2020-06-02 19:07:48\",\"id\":4}', '2020-06-03 02:07:48', '2020-06-03 02:07:48'),
(2, 1, 'system', 5, 'Prodcuts', 'Add new product (zenab)', '', '{\"cat_id\":\"2\",\"name\":\"zenab\",\"name_en\":\"\\u0632\\u064a\\u0646\\u0628\",\"description\":\"<p>rbhgrf<\\/p>\",\"brief\":\"etqg\",\"image\":\"\\/uploads\\/files\\/knife.jpg\",\"url\":\"rgqrg\",\"status\":\"1\",\"sort_order\":\"2\",\"updated_at\":\"2020-06-02 19:32:46\",\"created_at\":\"2020-06-02 19:32:46\",\"id\":5}', '2020-06-03 02:32:56', '2020-06-03 02:32:56'),
(3, 1, 'system', 6, 'Prodcuts', 'Add new product (هشام معاون)', '', '{\"cat_id\":\"3\",\"name\":\"\\u0647\\u0634\\u0627\\u0645 \\u0645\\u0639\\u0627\\u0648\\u0646\",\"name_en\":\"hisham\",\"description\":\"<p>yujthr<\\/p>\",\"brief\":\"etqg\",\"price\":\"36\",\"price_discount\":null,\"image\":\"\\/uploads\\/files\\/knife.jpg\",\"url\":\"rgqrg\",\"status\":\"1\",\"sort_order\":\"2\",\"updated_at\":\"2020-06-02 19:37:01\",\"created_at\":\"2020-06-02 19:37:01\",\"id\":6}', '2020-06-03 02:37:02', '2020-06-03 02:37:02'),
(4, 1, 'system', 7, 'Prodcuts', 'Add new product (زينب عيسى)', '', '{\"cat_id\":\"4\",\"name\":\"\\u0632\\u064a\\u0646\\u0628 \\u0639\\u064a\\u0633\\u0649\",\"name_en\":\"zenab essa\",\"description\":\"<p>grrgregr<\\/p>\",\"brief\":\"etqg\",\"price\":\"222\",\"price_discount\":\"4\",\"image\":\"\\/uploads\\/files\\/knife.jpg\",\"url\":\"rgqrg\",\"status\":0,\"sort_order\":\"1\",\"updated_at\":\"2020-06-02 19:40:44\",\"created_at\":\"2020-06-02 19:40:44\",\"id\":7}', '2020-06-03 02:40:45', '2020-06-03 02:40:45'),
(5, 1, 'system', 8, 'Prodcuts', 'Add new product (hisham)', '', '{\"cat_id\":\"3\",\"name\":\"hisham\",\"name_en\":\"\",\"description\":\"<p>5y45y<\\/p>\",\"brief\":\"etqg\",\"price\":\"1\",\"price_discount\":\"1\",\"image\":\"\",\"url\":\"rgqrg\",\"status\":0,\"sort_order\":\"1\",\"updated_at\":\"2020-06-02 19:42:40\",\"created_at\":\"2020-06-02 19:42:40\",\"id\":8}', '2020-06-03 02:42:41', '2020-06-03 02:42:41'),
(6, 1, 'system', 9, 'Prodcuts', 'Add new product (test)', '', '{\"cat_id\":\"3\",\"name\":\"test\",\"name_en\":\"7i6i57\",\"description\":\"<p>7i5i<\\/p>\",\"brief\":\"7i56\",\"origin\":\"4\",\"price\":\"1585\",\"price_discount\":\"12\",\"image\":\"\",\"url\":\"7i57i\",\"status\":\"1\",\"sort_order\":\"1285\",\"updated_at\":\"2020-06-02 19:58:33\",\"created_at\":\"2020-06-02 19:58:33\",\"id\":9}', '2020-06-03 02:58:33', '2020-06-03 02:58:33'),
(7, 1, 'system', 10, 'Prodcuts', 'Add new product (6y6jyjjyjyy)', '', '{\"cat_id\":\"4\",\"name\":\"6y6jyjjyjyy\",\"name_en\":\"6y6y\",\"description\":\"<p>yjyjy<\\/p>\",\"brief\":\"yjyj\",\"origin\":\"1\",\"price\":\"1\",\"price_discount\":\"1\",\"image\":\"\",\"url\":\"yjy\",\"status\":\"1\",\"sort_order\":\"1\",\"updated_at\":\"2020-06-02 19:59:14\",\"created_at\":\"2020-06-02 19:59:14\",\"id\":10}', '2020-06-03 02:59:14', '2020-06-03 02:59:14'),
(8, 1, 'system', 9, 'Product', 'Edit product (test)', '{\"id\":9,\"cat_id\":3,\"name\":\"test\",\"url\":\"7i57i\",\"image\":\"\",\"brief\":\"7i56\",\"description\":\"<p>7i5i<\\/p>\",\"price\":1585,\"price_discount\":12,\"colors\":null,\"models\":null,\"status\":1,\"sort_order\":1285,\"created_at\":\"2020-06-02 19:58:33\",\"updated_at\":\"2020-06-02 19:58:33\",\"name_en\":\"7i6i57\",\"origin\":4}', '{\"id\":9,\"cat_id\":\"3\",\"name\":\"test\",\"url\":\"7i57i\",\"image\":\"\",\"brief\":\"7i56\",\"description\":\"<p>7i5i<\\/p>\",\"price\":\"1585\",\"price_discount\":\"12\",\"colors\":null,\"models\":null,\"status\":\"1\",\"sort_order\":\"1\",\"created_at\":\"2020-06-02 19:58:33\",\"updated_at\":\"2020-06-02 20:17:39\",\"name_en\":null,\"origin\":null}', '2020-06-03 03:17:39', '2020-06-03 03:17:39'),
(9, 1, 'system', 9, 'Product', 'Edit product (test)', '{\"id\":9,\"cat_id\":3,\"name\":\"test\",\"url\":\"7i57i\",\"image\":\"\",\"brief\":\"7i56\",\"description\":\"<p>7i5i<\\/p>\",\"price\":1585,\"price_discount\":12,\"colors\":null,\"models\":null,\"status\":1,\"sort_order\":1,\"created_at\":\"2020-06-02 19:58:33\",\"updated_at\":\"2020-06-02 20:17:39\",\"name_en\":null,\"origin\":0}', '{\"id\":9,\"cat_id\":\"3\",\"name\":\"test\",\"url\":\"7i57i\",\"image\":\"\",\"brief\":\"7i56\",\"description\":\"<p>7i5i<\\/p>\",\"price\":\"1585\",\"price_discount\":\"12\",\"colors\":null,\"models\":null,\"status\":\"1\",\"sort_order\":\"1\",\"created_at\":\"2020-06-02 19:58:33\",\"updated_at\":\"2020-06-02 20:18:29\",\"name_en\":null,\"origin\":\"2\"}', '2020-06-03 03:18:30', '2020-06-03 03:18:30'),
(10, 1, 'system', 9, 'Product', 'Edit product (test)', '{\"id\":9,\"cat_id\":3,\"name\":\"test\",\"url\":\"7i57i\",\"image\":\"\",\"brief\":\"7i56\",\"description\":\"<p>7i5i<\\/p>\",\"price\":1585,\"price_discount\":12,\"colors\":null,\"models\":null,\"status\":1,\"sort_order\":1,\"created_at\":\"2020-06-02 19:58:33\",\"updated_at\":\"2020-06-02 20:18:29\",\"name_en\":null,\"origin\":2}', '{\"id\":9,\"cat_id\":\"3\",\"name\":\"test\",\"url\":\"7i57i\",\"image\":\"\",\"brief\":\"7i56\",\"description\":\"<p>7i5i<\\/p>\",\"price\":\"1585\",\"price_discount\":\"12\",\"colors\":null,\"models\":null,\"status\":\"1\",\"sort_order\":\"1\",\"created_at\":\"2020-06-02 19:58:33\",\"updated_at\":\"2020-06-02 20:18:57\",\"name_en\":null,\"origin\":\"3\"}', '2020-06-03 03:18:58', '2020-06-03 03:18:58'),
(11, 1, 'system', 9, 'Product', 'Edit product (test)', '{\"id\":9,\"cat_id\":3,\"name\":\"test\",\"url\":\"7i57i\",\"image\":\"\",\"brief\":\"7i56\",\"description\":\"<p>7i5i<\\/p>\",\"price\":1585,\"price_discount\":12,\"colors\":null,\"models\":null,\"status\":1,\"sort_order\":1,\"created_at\":\"2020-06-02 19:58:33\",\"updated_at\":\"2020-06-02 20:18:57\",\"name_en\":null,\"origin\":3}', '{\"id\":9,\"cat_id\":\"3\",\"name\":\"test\",\"url\":\"7i57i\",\"image\":\"\",\"brief\":\"7i56\",\"description\":\"<p>7i5i<\\/p>\",\"price\":\"1585\",\"price_discount\":\"12\",\"colors\":null,\"models\":null,\"status\":\"1\",\"sort_order\":\"1\",\"created_at\":\"2020-06-02 19:58:33\",\"updated_at\":\"2020-06-02 20:20:09\",\"name_en\":\"zenab essa\",\"origin\":\"3\"}', '2020-06-03 03:20:09', '2020-06-03 03:20:09'),
(12, 1, 'system', 9, 'Product', 'Edit product (test)', '{\"id\":9,\"cat_id\":3,\"name\":\"test\",\"url\":\"7i57i\",\"image\":\"\",\"brief\":\"7i56\",\"description\":\"<p>7i5i<\\/p>\",\"price\":1585,\"price_discount\":12,\"colors\":null,\"models\":null,\"status\":1,\"sort_order\":1,\"created_at\":\"2020-06-02 19:58:33\",\"updated_at\":\"2020-06-02 20:20:09\",\"name_en\":\"66y6y6y\",\"origin\":3}', '{\"id\":9,\"cat_id\":\"3\",\"name\":\"test\",\"url\":\"7i57i\",\"image\":\"\",\"brief\":\"7i56\",\"description\":\"<p>7i5i<\\/p>\",\"price\":\"1585\",\"price_discount\":\"12\",\"colors\":null,\"models\":null,\"status\":\"1\",\"sort_order\":\"1\",\"created_at\":\"2020-06-02 19:58:33\",\"updated_at\":\"2020-06-02 20:21:52\",\"name_en\":null,\"origin\":\"3\"}', '2020-06-03 03:21:52', '2020-06-03 03:21:52'),
(13, 1, 'system', 9, 'Product', 'Edit product (test)', '{\"id\":9,\"cat_id\":3,\"name\":\"test\",\"url\":\"7i57i\",\"image\":\"\",\"brief\":\"7i56\",\"description\":\"<p>7i5i<\\/p>\",\"price\":1585,\"price_discount\":12,\"colors\":null,\"models\":null,\"status\":1,\"sort_order\":1,\"created_at\":\"2020-06-02 19:58:33\",\"updated_at\":\"2020-06-02 20:21:52\",\"name_en\":null,\"origin\":3}', '{\"id\":9,\"cat_id\":\"3\",\"name\":\"test\",\"url\":\"7i57i\",\"image\":\"\",\"brief\":\"7i56\",\"description\":\"<p>7i5i<\\/p>\",\"price\":\"1585\",\"price_discount\":\"12\",\"colors\":null,\"models\":null,\"status\":\"1\",\"sort_order\":\"1\",\"created_at\":\"2020-06-02 19:58:33\",\"updated_at\":\"2020-06-02 20:22:34\",\"name_en\":\"jj7j7\",\"origin\":\"3\"}', '2020-06-03 03:22:34', '2020-06-03 03:22:34'),
(14, 1, 'system', 11, 'Prodcuts', 'Add new product (hisham)', '', '{\"cat_id\":\"2\",\"name\":\"hisham\",\"name_en\":\"zenab essa\",\"description\":\"<p>grgrg<\\/p>\",\"brief\":\"etqg\",\"origin\":\"7\",\"price\":\"1\",\"price_discount\":\"1\",\"image\":\"\\/uploads\\/files\\/12973483_1726075144272877_194223251258333372_o.jpg\",\"url\":\"rgqrg\",\"status\":\"1\",\"sort_order\":\"1\",\"updated_at\":\"2020-06-02 20:24:09\",\"created_at\":\"2020-06-02 20:24:09\",\"id\":11}', '2020-06-03 03:24:10', '2020-06-03 03:24:10'),
(15, 1, 'system', 12, 'Prodcuts', 'Add new product (هشام معاون)', '', '{\"cat_id\":\"2\",\"name\":\"\\u0647\\u0634\\u0627\\u0645 \\u0645\\u0639\\u0627\\u0648\\u0646\",\"name_en\":\"zenab essa\",\"description\":\"<p>thth<\\/p>\",\"brief\":\"etqg\",\"origin\":\"7\",\"price\":\"1\",\"price_discount\":\"1\",\"image\":\"\\/uploads\\/files\\/12973483_1726075144272877_194223251258333372_o.jpg\",\"url\":\"7i57i\",\"status\":\"1\",\"sort_order\":\"1\",\"updated_at\":\"2020-06-02 20:26:03\",\"created_at\":\"2020-06-02 20:26:03\",\"id\":12}', '2020-06-03 03:26:03', '2020-06-03 03:26:03'),
(16, 1, 'system', 12, 'Product', 'Edit product (هشام معاون)', '{\"id\":12,\"cat_id\":2,\"name\":\"\\u0647\\u0634\\u0627\\u0645 \\u0645\\u0639\\u0627\\u0648\\u0646\",\"url\":\"7i57i\",\"image\":\"\\/uploads\\/files\\/12973483_1726075144272877_194223251258333372_o.jpg\",\"brief\":\"etqg\",\"description\":\"<p>thth<\\/p>\",\"price\":1,\"price_discount\":1,\"colors\":null,\"models\":null,\"status\":1,\"sort_order\":1,\"created_at\":\"2020-06-02 20:26:03\",\"updated_at\":\"2020-06-02 20:26:03\",\"name_en\":\"zenab essa\",\"origin\":7}', '{\"id\":12,\"cat_id\":\"2\",\"name\":\"\\u0647\\u0634\\u0627\\u0645 \\u0645\\u0639\\u0627\\u0648\\u0646\",\"url\":\"7i57i\",\"image\":\"\\/uploads\\/files\\/12973483_1726075144272877_194223251258333372_o.jpg\",\"brief\":\"etqg\",\"description\":\"<p>thth<\\/p>\",\"price\":\"1\",\"price_discount\":\"1\",\"colors\":null,\"models\":null,\"status\":\"1\",\"sort_order\":\"1\",\"created_at\":\"2020-06-02 20:26:03\",\"updated_at\":\"2020-06-02 20:27:35\",\"name_en\":\"\",\"origin\":\"4\"}', '2020-06-03 03:27:35', '2020-06-03 03:27:35'),
(17, 1, 'system', 0, 'Prodcuts', 'Add new product ()', '', '{\"auther_id\":\"1\",\"translator_id\":\"1\",\"title\":\"first type\",\"type_new\":\"1\",\"depatement_id\":\"1\",\"date_publish\":\"2020-06-24\",\"updated_at\":\"2020-06-12 11:13:16\",\"created_at\":\"2020-06-12 11:13:16\",\"id\":0}', '2020-06-12 18:13:16', '2020-06-12 18:13:16'),
(18, 1, 'system', 0, 'Products', 'Delete product ()', '{\"id\":0,\"date_publish\":\"2020-06-24\",\"depatement_id\":1,\"auther_id\":1,\"translator_id\":1,\"title\":\"first type\",\"type_new\":1,\"updated_at\":\"2020-06-12 11:13:05\",\"created_at\":\"2020-06-12 11:13:05\"}', '', '2020-06-12 18:15:42', '2020-06-12 18:15:42'),
(19, 1, 'system', 1, 'Prodcuts', 'Add new product ()', '', '{\"auther_id\":\"1\",\"translator_id\":\"1\",\"title\":\"first type\",\"type_new\":\"1\",\"depatement_id\":\"1\",\"date_publish\":\"2020-06-02\",\"updated_at\":\"2020-06-12 11:16:45\",\"created_at\":\"2020-06-12 11:16:45\",\"id\":1}', '2020-06-12 18:16:45', '2020-06-12 18:16:45'),
(20, 1, 'system', 1, 'Products', 'Delete product ()', '{\"id\":1,\"date_publish\":\"2020-06-02\",\"depatement_id\":1,\"auther_id\":1,\"translator_id\":1,\"title\":\"first type\",\"type_new\":1,\"updated_at\":\"2020-06-12 11:16:45\",\"created_at\":\"2020-06-12 11:16:45\"}', '', '2020-06-12 18:17:05', '2020-06-12 18:17:05'),
(21, 1, 'system', 2, 'Prodcuts', 'Add new product ()', '', '{\"auther_id\":\"1\",\"translator_id\":\"1\",\"title\":\"first type\",\"type_new\":\"1\",\"depatement_id\":\"2\",\"date_publish\":\"2020-06-04\",\"updated_at\":\"2020-06-12 11:17:22\",\"created_at\":\"2020-06-12 11:17:22\",\"id\":2}', '2020-06-12 18:17:22', '2020-06-12 18:17:22'),
(22, 1, 'system', 2, 'Category', 'Add new category ()', '', '{\"date_publication\":\"2020-06-11\",\"title\":\"first type\",\"description\":\"u6yhtrge\",\"updated_at\":\"2020-06-12 11:42:34\",\"created_at\":\"2020-06-12 11:42:34\",\"id\":2}', '2020-06-12 18:42:34', '2020-06-12 18:42:34'),
(23, 1, 'system', 3, 'Category', 'Add new category ()', '', '{\"date_publication\":\"2020-06-11\",\"title\":\"first type\",\"description\":\"u6yhtrge\",\"updated_at\":\"2020-06-12 11:42:42\",\"created_at\":\"2020-06-12 11:42:42\",\"id\":3}', '2020-06-12 18:42:42', '2020-06-12 18:42:42'),
(24, 1, 'system', 1, 'Category', 'Edit category ()', '{\"id\":1,\"date_publication\":\"2020-06-11\",\"title\":\"first type\",\"description\":\"u6yhtrge\",\"updated_at\":\"2020-06-12 11:42:15\",\"created_at\":\"2020-06-12 11:42:15\"}', '{\"id\":1,\"date_publication\":null,\"title\":\"first typeuu6uuu\",\"description\":\"first type\",\"updated_at\":\"2020-06-12 12:41:39\",\"created_at\":\"2020-06-12 11:42:15\"}', '2020-06-12 19:41:39', '2020-06-12 19:41:39'),
(25, 1, 'system', 1, 'Category', 'Delete catefry ()', '{\"id\":1,\"date_publication\":\"0000-00-00\",\"title\":\"first typeuu6uuu\",\"description\":\"first type\",\"updated_at\":\"2020-06-12 12:41:39\",\"created_at\":\"2020-06-12 11:42:15\"}', '', '2020-06-12 19:42:01', '2020-06-12 19:42:01'),
(26, 1, 'system', 1, 'Category', 'Add new category ()', '', '{\"image\":\"\\/uploads\\/files\\/Capture_1.png\",\"auther_id\":\"2\",\"title\":\"first type\",\"translator_id\":\"1\",\"updated_at\":\"2020-06-12 13:22:11\",\"created_at\":\"2020-06-12 13:22:11\",\"id\":1}', '2020-06-12 20:22:13', '2020-06-12 20:22:13'),
(27, 1, 'system', 1, 'Category', 'Edit category ()', '{\"id\":1,\"image\":\"\\/uploads\\/files\\/Capture_1.\",\"auther_id\":2,\"title\":\"first kuykyukyukuyk\",\"translator_id\":1,\"updated_at\":\"2020-06-12 13:29:25\",\"created_at\":\"2020-06-12 13:22:11\"}', '{\"id\":1,\"image\":\"\\/uploads\\/files\\/Capture_1.\",\"auther_id\":\"2\",\"title\":\"first kuykyukyukuyk\",\"translator_id\":\"1\",\"updated_at\":\"2020-06-12 13:29:25\",\"created_at\":\"2020-06-12 13:22:11\"}', '2020-06-12 20:29:33', '2020-06-12 20:29:33'),
(28, 1, 'system', 1, 'Category', 'Edit category ()', '{\"id\":1,\"image\":\"\\/uploads\\/files\\/Capture_1.\",\"auther_id\":2,\"title\":\"first kuykyukyukuyk\",\"translator_id\":1,\"updated_at\":\"2020-06-12 13:29:25\",\"created_at\":\"2020-06-12 13:22:11\"}', '{\"id\":1,\"image\":\"\\/uploads\\/files\\/Capture_1.\",\"auther_id\":\"2\",\"title\":\"first h\",\"translator_id\":\"1\",\"updated_at\":\"2020-06-12 13:30:33\",\"created_at\":\"2020-06-12 13:22:11\"}', '2020-06-12 20:30:33', '2020-06-12 20:30:33'),
(29, 1, 'system', 1, 'Category', 'Delete catefry ()', '{\"id\":1,\"image\":\"\\/uploads\\/files\\/Capture_1.\",\"auther_id\":2,\"title\":\"first h\",\"translator_id\":1,\"updated_at\":\"2020-06-12 13:30:33\",\"created_at\":\"2020-06-12 13:22:11\"}', '', '2020-06-12 20:31:15', '2020-06-12 20:31:15'),
(30, 1, 'system', 2, 'Category', 'Add new category ()', '', '{\"image\":\"\\/uploads\\/files\\/Capture_1.png\",\"auther_id\":\"1\",\"title\":\"first type5t5t\",\"translator_id\":\"1\",\"updated_at\":\"2020-06-12 13:31:35\",\"created_at\":\"2020-06-12 13:31:35\",\"id\":2}', '2020-06-12 20:31:35', '2020-06-12 20:31:35'),
(31, 1, 'system', 2, 'Category', 'Delete catefry ()', '{\"id\":2,\"image\":\"\\/uploads\\/files\\/Capture_1.\",\"auther_id\":1,\"title\":\"first type5t5t\",\"translator_id\":1,\"updated_at\":\"2020-06-12 13:31:35\",\"created_at\":\"2020-06-12 13:31:35\"}', '', '2020-06-12 20:31:39', '2020-06-12 20:31:39'),
(32, 1, 'system', 2, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"main_id\":\"2\",\"branch_id\":\"2\",\"professor\":\"iuyythtg\",\"title\":\"first type5t5t\",\"date\":\"2020-06-24\",\"updated_at\":\"2020-06-13 08:26:08\",\"created_at\":\"2020-06-13 08:26:08\",\"id\":2}', '2020-06-13 15:26:09', '2020-06-13 15:26:09'),
(33, 1, 'system', 1, 'Panels', 'Delete panel ()', '{\"id\":1,\"image\":\"\\/uploads\\/files\\/vcredist.b\",\"main_id\":2,\"branch_id\":2,\"professor\":\"iuyythtg\",\"title\":\"first type5t5t\",\"date\":\"2020-06-24\",\"updated_at\":\"2020-06-13 08:25:57\",\"created_at\":\"1970-01-01 00:33:40\"}', '', '2020-06-13 15:26:58', '2020-06-13 15:26:58'),
(34, 1, 'system', 2, 'Panels', 'Delete panel ()', '{\"id\":2,\"image\":\"\\/uploads\\/files\\/vcredist.b\",\"main_id\":2,\"branch_id\":2,\"professor\":\"iuyythtg\",\"title\":\"first type5t5t\",\"date\":\"2020-06-24\",\"updated_at\":\"2020-06-13 08:26:08\",\"created_at\":\"1970-01-01 00:33:40\"}', '', '2020-06-13 15:27:21', '2020-06-13 15:27:21'),
(35, 1, 'system', 3, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"main_id\":\"1\",\"branch_id\":\"2\",\"professor\":\"iuyythtg\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-03\",\"updated_at\":\"2020-06-13 08:27:35\",\"created_at\":\"2020-06-13 08:27:35\",\"id\":3}', '2020-06-13 15:27:35', '2020-06-13 15:27:35'),
(36, 1, 'system', 3, 'Panels', 'Edit panel ()', '{\"id\":3,\"group_id\":1,\"name\":\"Banner 3\",\"ident\":\"\",\"title\":\"Banner 3\",\"content\":\"\",\"code\":\"\",\"photo\":\"\\/uploads\\/files\\/daniel_deadsea.jpg\",\"url\":\"\",\"status\":0,\"sort_order\":3,\"created_at\":\"2017-03-23 04:08:21\",\"updated_at\":\"2017-04-09 21:25:44\"}', '{\"id\":3,\"group_id\":null,\"name\":null,\"ident\":null,\"title\":\"first kuykyukyukuyk\",\"content\":null,\"code\":null,\"photo\":null,\"url\":null,\"status\":null,\"sort_order\":null,\"created_at\":\"2017-03-23 04:08:21\",\"updated_at\":\"2020-06-13 08:31:36\"}', '2020-06-13 15:31:37', '2020-06-13 15:31:37'),
(37, 1, 'system', 3, 'Panels', 'Edit panel ()', '{\"id\":3,\"image\":\"\\/uploads\\/files\\/vcredist.b\",\"main_id\":1,\"branch_id\":2,\"professor\":\"iuyythtg\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-03\",\"updated_at\":\"2020-06-13 08:27:35\",\"created_at\":\"1970-01-01 00:33:40\"}', '{\"id\":3,\"image\":\"\\/uploads\\/files\\/vcredist.b\",\"main_id\":\"1\",\"branch_id\":\"2\",\"professor\":\"hisham\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-03\",\"updated_at\":\"2020-06-13 08:32:16\",\"created_at\":\"1970-01-01 00:33:40\"}', '2020-06-13 15:32:17', '2020-06-13 15:32:17'),
(38, 1, 'system', 3, 'Panels', 'Edit panel ()', '{\"id\":3,\"image\":\"\\/uploads\\/files\\/vcredist.b\",\"main_id\":1,\"branch_id\":2,\"professor\":\"hisham\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-03\",\"updated_at\":\"2020-06-13 08:32:16\",\"created_at\":\"1970-01-01 00:33:40\"}', '{\"id\":3,\"image\":\"\\/uploads\\/files\\/vcredist.b\",\"main_id\":\"1\",\"branch_id\":\"2\",\"professor\":\"hishamee\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-03\",\"updated_at\":\"2020-06-13 08:33:01\",\"created_at\":\"1970-01-01 00:33:40\"}', '2020-06-13 15:33:01', '2020-06-13 15:33:01'),
(39, 1, 'system', 3, 'Panels', 'Edit panel ()', '{\"id\":3,\"image\":\"\\/uploads\\/files\\/vcredist.b\",\"main_id\":1,\"branch_id\":2,\"professor\":\"hishamee\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-03\",\"updated_at\":\"2020-06-13 08:33:01\",\"created_at\":\"1970-01-01 00:33:40\"}', '{\"id\":3,\"image\":\"\\/uploads\\/files\\/vcredist.b\",\"main_id\":\"1\",\"branch_id\":\"2\",\"professor\":\"hisham\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-03\",\"updated_at\":\"2020-06-13 08:33:38\",\"created_at\":\"1970-01-01 00:33:40\"}', '2020-06-13 15:33:38', '2020-06-13 15:33:38'),
(40, 1, 'system', 3, 'Panels', 'Delete panel ()', '{\"id\":3,\"image\":\"\\/uploads\\/files\\/vcredist.b\",\"main_id\":1,\"branch_id\":2,\"professor\":\"hisham\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-03\",\"updated_at\":\"2020-06-13 08:33:38\",\"created_at\":\"1970-01-01 00:33:40\"}', '', '2020-06-13 15:33:52', '2020-06-13 15:33:52'),
(41, 1, 'system', 1, 'Panel', 'Add new panel ()', '', '{\"image\":\"\",\"main_id\":\"1\",\"branch_id\":\"2\",\"professor\":\"iuyythtg\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-08\",\"updated_at\":\"2020-06-13 11:04:15\",\"created_at\":\"2020-06-13 11:04:15\",\"id\":1}', '2020-06-13 18:04:16', '2020-06-13 18:04:16'),
(42, 1, 'system', 2, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"main_id\":\"1\",\"branch_id\":\"1\",\"professor\":\"hisham\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-01\",\"updated_at\":\"2020-06-13 11:37:23\",\"created_at\":\"2020-06-13 11:37:23\",\"id\":2}', '2020-06-13 18:37:23', '2020-06-13 18:37:23'),
(43, 1, 'system', 3, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"main_id\":\"1\",\"branch_id\":\"2\",\"professor\":\"iuyythtg\",\"title\":\"yubhnijmk,l.;\",\"date\":\"2020-06-12\",\"updated_at\":\"2020-06-13 11:38:24\",\"created_at\":\"2020-06-13 11:38:24\",\"id\":3}', '2020-06-13 18:38:24', '2020-06-13 18:38:24'),
(44, 1, 'system', 4, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"main_id\":\"1\",\"branch_id\":\"1\",\"professor\":\"hisham\",\"title\":\"yubhnijmk,l.;\",\"date\":\"2020-06-07\",\"updated_at\":\"2020-06-13 11:39:13\",\"created_at\":\"2020-06-13 11:39:13\",\"id\":4}', '2020-06-13 18:39:14', '2020-06-13 18:39:14'),
(45, 1, 'system', 5, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"main_id\":\"2\",\"branch_id\":\"1\",\"professor\":\"hisham\",\"title\":\"first typeuu6uuu\",\"date\":\"2020-06-07\",\"updated_at\":\"2020-06-13 11:39:39\",\"created_at\":\"2020-06-13 11:39:39\",\"id\":5}', '2020-06-13 18:39:39', '2020-06-13 18:39:39'),
(46, 1, 'system', 6, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"main_id\":\"2\",\"branch_id\":\"2\",\"professor\":\"iuyythtg\",\"title\":\"first type5t5t\",\"date\":\"2020-06-10\",\"updated_at\":\"2020-06-13 11:40:24\",\"created_at\":\"2020-06-13 11:40:24\",\"id\":6}', '2020-06-13 18:40:24', '2020-06-13 18:40:24'),
(47, 1, 'system', 1, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"main_id\":\"1\",\"branch_id\":\"2\",\"title\":\"yubhnijmk,l.;\",\"date\":\"2020-06-02\",\"type_id\":\"2\",\"updated_at\":\"2020-06-13 11:42:28\",\"created_at\":\"2020-06-13 11:42:28\",\"id\":1}', '2020-06-13 18:42:29', '2020-06-13 18:42:29'),
(48, 1, 'system', 2, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"main_id\":\"1\",\"branch_id\":\"2\",\"title\":\"yubhnijmk,l.;\",\"date\":\"2020-06-02\",\"type_id\":\"2\",\"updated_at\":\"2020-06-13 11:42:49\",\"created_at\":\"2020-06-13 11:42:49\",\"id\":2}', '2020-06-13 18:42:49', '2020-06-13 18:42:49'),
(49, 1, 'system', 1, 'Panels', 'Delete panel ()', '{\"id\":1,\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"main_id\":1,\"branch_id\":2,\"type_id\":2,\"date\":\"2020-06-02\",\"title\":\"yubhnijmk,l.;\",\"updated_at\":\"2020-06-13 11:42:28\",\"created_at\":\"2020-06-13 11:42:28\"}', '', '2020-06-13 18:44:30', '2020-06-13 18:44:30'),
(50, 1, 'system', 2, 'Panels', 'Delete panel ()', '{\"id\":2,\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"main_id\":1,\"branch_id\":2,\"type_id\":2,\"date\":\"2020-06-02\",\"title\":\"yubhnijmk,l.;\",\"updated_at\":\"2020-06-13 11:42:49\",\"created_at\":\"2020-06-13 11:42:49\"}', '', '2020-06-13 18:45:50', '2020-06-13 18:45:50'),
(51, 1, 'system', 3, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/Capture_1.png\",\"main_id\":\"1\",\"branch_id\":\"1\",\"title\":\"first typeuu6uuu\",\"date\":\"2020-06-11\",\"type_id\":\"1\",\"updated_at\":\"2020-06-13 11:46:08\",\"created_at\":\"2020-06-13 11:46:08\",\"id\":3}', '2020-06-13 18:46:08', '2020-06-13 18:46:08'),
(52, 1, 'system', 3, 'Panels', 'Edit panel ()', '{\"id\":3,\"image\":\"\\/uploads\\/files\\/Capture_1.png\",\"main_id\":1,\"branch_id\":1,\"type_id\":1,\"date\":\"2020-06-11\",\"title\":\"dd\",\"updated_at\":\"2020-06-13 11:57:49\",\"created_at\":\"2020-06-13 11:46:08\"}', '{\"id\":3,\"image\":\"\\/uploads\\/files\\/Capture_1.png\",\"main_id\":\"1\",\"branch_id\":\"1\",\"type_id\":\"1\",\"date\":\"2020-06-11\",\"title\":\"ddf\",\"updated_at\":\"2020-06-13 11:59:32\",\"created_at\":\"2020-06-13 11:46:08\"}', '2020-06-13 18:59:32', '2020-06-13 18:59:32'),
(53, 1, 'system', 3, 'Panels', 'Delete panel ()', '{\"id\":3,\"image\":\"\\/uploads\\/files\\/Capture_1.png\",\"main_id\":1,\"branch_id\":1,\"type_id\":1,\"date\":\"2020-06-11\",\"title\":\"ddf\",\"updated_at\":\"2020-06-13 11:59:32\",\"created_at\":\"2020-06-13 11:46:08\"}', '', '2020-06-13 18:59:47', '2020-06-13 18:59:47'),
(54, 1, 'system', 0, 'Panel', 'Add new panel (test)', '', '{\"name\":\"test\",\"updated_at\":\"2020-06-14 18:12:41\",\"created_at\":\"2020-06-14 18:12:41\",\"id\":0}', '2020-06-15 01:12:41', '2020-06-15 01:12:41'),
(55, 1, 'system', 1, 'Panels', 'Edit panel (hisham)', '{\"id\":1,\"name\":\"test\",\"updated_at\":\"2020-06-14 18:12:41\",\"created_at\":\"2020-06-14 18:12:41\"}', '{\"id\":1,\"name\":\"hisham\",\"updated_at\":\"2020-06-14 19:03:05\",\"created_at\":\"2020-06-14 18:12:41\"}', '2020-06-15 02:03:05', '2020-06-15 02:03:05'),
(56, 1, 'system', 1, 'Panels', 'Delete panel (hisham)', '{\"id\":1,\"name\":\"hisham\",\"updated_at\":\"2020-06-14 19:03:05\",\"created_at\":\"2020-06-14 18:12:41\"}', '', '2020-06-15 02:04:12', '2020-06-15 02:04:12'),
(57, 1, 'system', 1, 'Category', 'Add new category ()', '', '{\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"auther_id\":\"1\",\"title\":\"first type\",\"translator_id\":\"1\",\"updated_at\":\"2020-06-14 19:09:27\",\"created_at\":\"2020-06-14 19:09:27\",\"id\":1}', '2020-06-15 02:09:27', '2020-06-15 02:09:27'),
(58, 1, 'system', 7, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/Capture_1.png\",\"main_id\":\"2\",\"branch_id\":\"1\",\"professor\":\"iuyythtg\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-20\",\"updated_at\":\"2020-06-14 19:55:21\",\"created_at\":\"2020-06-14 19:55:21\",\"id\":7}', '2020-06-15 02:55:21', '2020-06-15 02:55:21'),
(59, 1, 'system', 1, 'Panels', 'Delete panel ()', '{\"id\":1,\"image\":\"\",\"main_id\":1,\"branch_id\":2,\"professor\":\"iuyythtg\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-08\",\"updated_at\":\"2020-06-13 11:04:15\",\"created_at\":\"1970-01-01 00:33:40\"}', '', '2020-06-15 02:57:20', '2020-06-15 02:57:20'),
(60, 1, 'system', 2, 'Panels', 'Edit panel ()', '{\"id\":2,\"image\":\"\\/uploads\\/files\\/vcredist.b\",\"main_id\":1,\"branch_id\":1,\"professor\":\"hisham\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-01\",\"updated_at\":\"2020-06-13 11:37:23\",\"created_at\":\"1970-01-01 00:33:40\"}', '{\"id\":2,\"image\":\"\\/uploads\\/files\\/vcredist.b\",\"main_id\":\"3\",\"branch_id\":\"1\",\"professor\":\"hisham\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-01\",\"updated_at\":\"2020-06-14 19:59:14\",\"created_at\":\"1970-01-01 00:33:40\"}', '2020-06-15 02:59:14', '2020-06-15 02:59:14'),
(61, 1, 'system', 2, 'Panels', 'Edit panel ()', '{\"id\":2,\"image\":\"\\/uploads\\/files\\/vcredist.b\",\"main_id\":3,\"branch_id\":1,\"professor\":\"hisham\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-01\",\"updated_at\":\"2020-06-14 19:59:14\",\"created_at\":\"1970-01-01 00:33:40\"}', '{\"id\":2,\"image\":\"\\/uploads\\/files\\/vcredist.b\",\"main_id\":\"3\",\"branch_id\":\"1\",\"professor\":\"hisham\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-01\",\"updated_at\":\"2020-06-14 19:59:14\",\"created_at\":\"1970-01-01 00:33:40\"}', '2020-06-15 02:59:25', '2020-06-15 02:59:25'),
(62, 1, 'system', 1, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"main_id\":\"3\",\"branch_id\":\"1\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-16\",\"type_id\":\"1\",\"updated_at\":\"2020-06-14 20:06:18\",\"created_at\":\"2020-06-14 20:06:18\",\"id\":1}', '2020-06-15 03:06:19', '2020-06-15 03:06:19'),
(63, 1, 'system', 1, 'Panel', 'Add new panel (hisham)', '', '{\"name\":\"hisham\",\"updated_at\":\"2020-06-15 18:31:10\",\"created_at\":\"2020-06-15 18:31:10\",\"id\":1}', '2020-06-16 01:31:10', '2020-06-16 01:31:10'),
(64, 1, 'system', 2, 'Panel', 'Add new panel (hisham)', '', '{\"name\":\"hisham\",\"updated_at\":\"2020-06-15 18:31:53\",\"created_at\":\"2020-06-15 18:31:53\",\"id\":2}', '2020-06-16 01:31:53', '2020-06-16 01:31:53'),
(65, 1, 'system', 3, 'Panel', 'Add new panel (hisham)', '', '{\"name\":\"hisham\",\"updated_at\":\"2020-06-15 18:32:14\",\"created_at\":\"2020-06-15 18:32:14\",\"id\":3}', '2020-06-16 01:32:15', '2020-06-16 01:32:15'),
(66, 1, 'system', 2, 'Panels', 'Delete panel (0)', '{\"id\":2,\"name\":\"0\",\"updated_at\":\"2020-06-15 18:31:53\",\"created_at\":\"2020-06-15 18:31:53\"}', '', '2020-06-16 01:32:59', '2020-06-16 01:32:59'),
(67, 1, 'system', 1, 'Panels', 'Delete panel (0)', '{\"id\":1,\"name\":\"0\",\"updated_at\":\"2020-06-15 18:31:10\",\"created_at\":\"2020-06-15 18:31:10\"}', '', '2020-06-16 01:33:04', '2020-06-16 01:33:04'),
(68, 1, 'system', 3, 'Panels', 'Edit panel (Hisham muawen)', '{\"id\":3,\"name\":\"hisham\",\"updated_at\":\"2020-06-15 18:32:14\",\"created_at\":\"2020-06-15 18:32:14\"}', '{\"id\":3,\"name\":\"Hisham muawen\",\"updated_at\":\"2020-06-15 18:34:27\",\"created_at\":\"2020-06-15 18:32:14\"}', '2020-06-16 01:34:27', '2020-06-16 01:34:27'),
(69, 1, 'system', 1, 'Category', 'Add new category ()', '', '{\"number\":\"454\",\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"updated_at\":\"2020-06-15 19:01:59\",\"created_at\":\"2020-06-15 19:01:59\",\"id\":1}', '2020-06-16 02:01:59', '2020-06-16 02:01:59'),
(70, 1, 'system', 2, 'Category', 'Add new category ()', '', '{\"number\":\"33\",\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"updated_at\":\"2020-06-15 19:03:20\",\"created_at\":\"2020-06-15 19:03:20\",\"id\":2}', '2020-06-16 02:03:20', '2020-06-16 02:03:20'),
(71, 1, 'system', 2, 'Category', 'Delete catefry ()', '{\"id\":2,\"date_publication\":\"2020-06-11\",\"title\":\"first type\",\"description\":\"u6yhtrge\",\"updated_at\":\"2020-06-12 11:42:34\",\"created_at\":\"2020-06-12 11:42:34\"}', '', '2020-06-16 02:03:25', '2020-06-16 02:03:25'),
(72, 1, 'system', 1, 'Category', 'Delete catefry ()', '{\"id\":1,\"number\":454,\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"updated_at\":\"2020-06-15 19:01:59\",\"created_at\":\"2020-06-15 19:01:59\"}', '', '2020-06-16 02:06:04', '2020-06-16 02:06:04'),
(73, 1, 'system', 3, 'Category', 'Edit category ()', '{\"id\":2,\"number\":33,\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"updated_at\":\"2020-06-15 19:03:20\",\"created_at\":\"2020-06-15 19:03:20\"}', '{\"number\":\"333\",\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"updated_at\":\"2020-06-15 19:07:53\",\"created_at\":\"2020-06-15 19:07:53\",\"id\":3}', '2020-06-16 02:07:53', '2020-06-16 02:07:53'),
(74, 1, 'system', 3, 'Category', 'Delete catefry ()', '{\"id\":3,\"number\":333,\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"updated_at\":\"2020-06-15 19:07:53\",\"created_at\":\"2020-06-15 19:07:53\"}', '', '2020-06-16 02:08:06', '2020-06-16 02:08:06'),
(75, 1, 'system', 1, 'Category', 'Add new category ()', '', '{\"name_ar\":\"\\u062b\\u0642\\u064a\",\"name_en\":\"\\u062b\\u0628\\u0635\",\"main_auther\":\"1\",\"date_of_publication\":\"2020-06-09\",\"ISBN\":\"334\",\"lang_id\":\"1\",\"Edition\":\"\\u0644\\u0641\\u0641\\u0644\\u0641\",\"original_publisher\":\"\\u0628\\u0628\\u0628\\u0644\\u0628\",\"cat_id\":\"3\",\"number_page\":\"44\",\"dimensions\":\"151551\",\"Available_versions\":\"\\u0628\\u0628\\u0628\",\"ISBN_ELECTRONIC\":\"434\",\"symbol_book\":\"432\",\"price\":\"343\",\"currency_id\":\"1\",\"about_book\":\"\\u0639\\u0641\\u0649\\u062b\\u063a\\u0642\\u0644\\u0627\\u0644\\u0631\\u0628\",\"about_auther\":\"\\u0631\\u062b\\u0628\\u0624\\u064a\\u0633\",\"about_translator\":\"\\u0642\\u0644\\u0627\\u0641\\u0631\\u062b\\u0628\\u0624\",\"image_book\":\"\",\"image_auther\":\"\",\"image_translator\":\"\",\"evaluation\":\"3\",\"discount_price\":\"43\",\"updated_at\":\"2020-06-15 19:50:37\",\"created_at\":\"2020-06-15 19:50:37\",\"id\":1}', '2020-06-16 02:50:37', '2020-06-16 02:50:37'),
(76, 1, 'system', 2, 'Category', 'Add new category ()', '', '{\"name_ar\":\"\\u062b\\u0642\\u064a\",\"name_en\":\"jj7j7\",\"date_of_publication\":\"2020-06-17\",\"ISBN\":\"232\",\"lang_id\":\"2\",\"Edition\":\"\\u0644\\u0641\\u0641\\u0644\\u0641\",\"original_publisher\":\"hthr\",\"cat_id\":\"4\",\"number_page\":\"332\",\"dimensions\":\"151551\",\"Available_versions\":\"1\",\"ISBN_ELECTRONIC\":\"32\",\"symbol_book\":\"232\",\"price\":\"3232\",\"currency_id\":\"2\",\"about_book\":\"fgeg\",\"about_auther\":\"ergerg\",\"about_translator\":\"ergr\",\"image_book\":\"hisham.jpg\",\"image_auther\":\"hisham.jpg\",\"image_translator\":\"hisham.jpg\",\"evaluation\":\"2323\",\"discount_price\":\"3232\",\"updated_at\":\"2020-06-19 11:29:14\",\"created_at\":\"2020-06-19 11:29:14\",\"id\":2}', '2020-06-19 18:29:15', '2020-06-19 18:29:15'),
(77, 1, 'system', 3, 'Category', 'Add new category ()', '', '{\"name_ar\":\"\\u062b\\u0642\\u064a\",\"name_en\":\"4eff\",\"date_of_publication\":\"2020-06-03\",\"ISBN\":\"323\",\"lang_id\":\"1\",\"Edition\":\"\\u0627\\u0644\\u0623\\u0648\\u0644\\u0649\",\"original_publisher\":\"2\",\"cat_id\":\"4\",\"number_page\":\"320\",\"dimensions\":\"3.*5*3\",\"Available_versions\":\"1\",\"ISBN_ELECTRONIC\":\"23232\",\"symbol_book\":\"3232\",\"price\":\"323232\",\"currency_id\":\"1\",\"about_book\":\"\\u0642\\u0644\\u062b\",\"about_auther\":\"\",\"about_translator\":\"\\u0644\\u062b\\u0644\\u0642\\u0644\",\"image_book\":\"hisham.jpg\",\"image_auther\":\"hisham.jpg\",\"image_translator\":\"hisham.jpg\",\"evaluation\":\"4\",\"discount_price\":\"2\",\"updated_at\":\"2020-06-19 11:35:04\",\"created_at\":\"2020-06-19 11:35:04\",\"id\":3}', '2020-06-19 18:35:04', '2020-06-19 18:35:04'),
(78, 1, 'system', 1, 'Category', 'Delete catefry ()', '{\"id\":1,\"name_ar\":\"\\u062b\\u0642\\u064a\",\"name_en\":\"\\u062b\\u0628\\u0635\",\"date_of_publication\":\"2020-06-09\",\"ISBN\":334,\"lang_id\":1,\"Edition\":0,\"original_publisher\":0,\"cat_id\":3,\"number_page\":44,\"dimensions\":151551,\"Available_versions\":0,\"ISBN_ELECTRONIC\":434,\"ISBN_PRINTED\":0,\"symbol_book\":432,\"price\":343,\"currency_id\":1,\"about_book\":\"\\u0639\\u0641\\u0649\\u062b\\u063a\\u0642\\u0644\\u0627\\u0644\\u0631\\u0628\",\"about_auther\":\"\\u0631\\u062b\\u0628\\u0624\\u064a\\u0633\",\"about_translator\":\"\\u0642\\u0644\\u0627\\u0641\\u0631\\u062b\\u0628\\u0624\",\"image_book\":\"\",\"image_auther\":\"\",\"image_translator\":\"\",\"evaluation\":3,\"discount_price\":43,\"updated_at\":\"2020-06-15 19:50:37\",\"created_at\":\"2020-06-15 19:50:37\"}', '', '2020-06-19 18:35:47', '2020-06-19 18:35:47'),
(79, 1, 'system', 2, 'Category', 'Edit category ()', '{\"id\":2,\"name_ar\":\"\\u062b\\u0642\\u064a\",\"name_en\":\"jj7j7\",\"date_of_publication\":\"2020-06-17\",\"ISBN\":232,\"lang_id\":2,\"Edition\":0,\"original_publisher\":0,\"cat_id\":4,\"number_page\":332,\"dimensions\":151551,\"Available_versions\":1,\"ISBN_ELECTRONIC\":32,\"ISBN_PRINTED\":0,\"symbol_book\":232,\"price\":3232,\"currency_id\":2,\"about_book\":\"fgeg\",\"about_auther\":\"ergerg\",\"about_translator\":\"ergr\",\"image_book\":\"hisham.jpg\",\"image_auther\":\"hisham.jpg\",\"image_translator\":\"hisham.jpg\",\"evaluation\":2323,\"discount_price\":3232,\"updated_at\":\"2020-06-19 11:29:14\",\"created_at\":\"2020-06-19 11:29:14\"}', '{\"id\":2,\"name_ar\":\"\\u062b\\u0642\\u064a\",\"name_en\":\"jj7j7\",\"date_of_publication\":\"2020-06-17\",\"ISBN\":\"232\",\"lang_id\":\"2\",\"Edition\":\"0\",\"original_publisher\":\"1\",\"cat_id\":\"4\",\"number_page\":\"332\",\"dimensions\":\"151551\",\"Available_versions\":\"1\",\"ISBN_ELECTRONIC\":\"32\",\"ISBN_PRINTED\":0,\"symbol_book\":\"232\",\"price\":\"3232\",\"currency_id\":\"2\",\"about_book\":\"fgeg\",\"about_auther\":\"ergerg\",\"about_translator\":\"ergr\",\"image_book\":\"\",\"image_auther\":\"\",\"image_translator\":\"\",\"evaluation\":\"2323\",\"discount_price\":\"3232\",\"updated_at\":\"2020-06-19 11:51:56\",\"created_at\":\"2020-06-19 11:29:14\"}', '2020-06-19 18:51:56', '2020-06-19 18:51:56'),
(80, 1, 'system', 2, 'Category', 'Edit category ()', '{\"id\":2,\"name_ar\":\"\\u062b\\u0642\\u064a\",\"name_en\":\"jj7j7\",\"date_of_publication\":\"2020-06-17\",\"ISBN\":232,\"lang_id\":2,\"Edition\":0,\"original_publisher\":1,\"cat_id\":4,\"number_page\":332,\"dimensions\":151551,\"Available_versions\":1,\"ISBN_ELECTRONIC\":32,\"ISBN_PRINTED\":0,\"symbol_book\":232,\"price\":3232,\"currency_id\":2,\"about_book\":\"fgeg\",\"about_auther\":\"ergerg\",\"about_translator\":\"ergr\",\"image_book\":\"\",\"image_auther\":\"\",\"image_translator\":\"\",\"evaluation\":2323,\"discount_price\":3232,\"updated_at\":\"2020-06-19 11:51:56\",\"created_at\":\"2020-06-19 11:29:14\"}', '{\"id\":2,\"name_ar\":\"\\u062b\\u0642\\u064a\",\"name_en\":\"ytyyyyy\",\"date_of_publication\":\"2020-06-17\",\"ISBN\":\"232\",\"lang_id\":\"2\",\"Edition\":\"0\",\"original_publisher\":\"1\",\"cat_id\":\"4\",\"number_page\":\"332\",\"dimensions\":\"151551\",\"Available_versions\":\"1\",\"ISBN_ELECTRONIC\":\"32\",\"ISBN_PRINTED\":0,\"symbol_book\":\"232\",\"price\":\"3232\",\"currency_id\":\"2\",\"about_book\":\"fgeg\",\"about_auther\":\"ergerg\",\"about_translator\":\"ergr\",\"image_book\":\"\",\"image_auther\":\"\",\"image_translator\":\"\",\"evaluation\":\"2323\",\"discount_price\":\"3232\",\"updated_at\":\"2020-06-19 11:52:23\",\"created_at\":\"2020-06-19 11:29:14\"}', '2020-06-19 18:52:23', '2020-06-19 18:52:23'),
(81, 1, 'system', 18, 'Category', 'Add new category ()', '', '{\"name_ar\":\"ewf\",\"name_en\":\"efwefewfefewfwee\",\"date_of_publication\":\"2020-06-08\",\"ISBN\":\"233\",\"lang_id\":\"2\",\"Edition\":\"\\u0627\\u0644\\u0623\\u0648\\u0644\\u0649\",\"original_publisher\":\"1\",\"cat_id\":\"5\",\"number_page\":\"323\",\"dimensions\":\"3.*5*3\",\"Available_versions\":\"1\",\"ISBN_ELECTRONIC\":\"233\",\"symbol_book\":\"2332\",\"price\":\"23323\",\"currency_id\":\"2\",\"about_book\":\"2ffrfwf\",\"about_auther\":\"efwefe\",\"about_translator\":\"\",\"image_book\":\"hisham.jpg\",\"image_auther\":\"hisham.jpg\",\"image_translator\":\"hisham.jpg\",\"evaluation\":\"3\",\"discount_price\":\"1\",\"updated_at\":\"2020-06-19 12:05:06\",\"created_at\":\"2020-06-19 12:05:06\",\"id\":18}', '2020-06-19 19:05:06', '2020-06-19 19:05:06'),
(82, 1, 'system', 19, 'Category', 'Add new category ()', '', '{\"name_ar\":\"\\u062b\\u0642\\u064a\",\"name_en\":\"r3\",\"date_of_publication\":\"2020-06-10\",\"ISBN\":\"232\",\"lang_id\":\"2\",\"Edition\":\"\\u0627\\u0644\\u0623\\u0648\\u0644\\u0649\",\"original_publisher\":\"1\",\"cat_id\":\"4\",\"number_page\":\"232\",\"dimensions\":\"151551\",\"Available_versions\":\"1\",\"ISBN_ELECTRONIC\":\"23\",\"symbol_book\":\"322\",\"price\":\"232\",\"currency_id\":\"2\",\"about_book\":\"fefew\",\"about_auther\":\"efwfe\",\"about_translator\":\"fwf\",\"image_book\":\"hisham.jpg\",\"image_auther\":\"hisham.jpg\",\"image_translator\":\"hisham.jpg\",\"evaluation\":\"232\",\"discount_price\":\"32\",\"updated_at\":\"2020-06-19 12:06:26\",\"created_at\":\"2020-06-19 12:06:26\",\"id\":19}', '2020-06-19 19:06:27', '2020-06-19 19:06:27'),
(83, 1, 'system', 22, 'Category', 'Add new category ()', '', '{\"name_ar\":\"\",\"name_en\":\"\",\"date_of_publication\":\"\",\"ISBN\":\"\",\"lang_id\":\"2\",\"Edition\":\"\\u0644\\u0641\\u0641\\u0644\\u0641\",\"original_publisher\":\"0\",\"cat_id\":\"4\",\"number_page\":\"\",\"dimensions\":\"151551\",\"Available_versions\":\"0\",\"ISBN_ELECTRONIC\":\"\",\"symbol_book\":\"\",\"price\":\"\",\"currency_id\":\"1\",\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_book\":\"hisham.jpg\",\"image_auther\":\"hisham.jpg\",\"image_translator\":\"hisham.jpg\",\"evaluation\":\"\",\"discount_price\":\"\",\"updated_at\":\"2020-06-19 12:08:11\",\"created_at\":\"2020-06-19 12:08:11\",\"id\":22}', '2020-06-19 19:08:11', '2020-06-19 19:08:11'),
(84, 1, 'system', 23, 'Category', 'Add new category ()', '', '{\"name_ar\":\"\",\"name_en\":\"\",\"date_of_publication\":\"\",\"ISBN\":\"\",\"lang_id\":\"1\",\"Edition\":\"\",\"original_publisher\":\"0\",\"cat_id\":\"2\",\"number_page\":\"\",\"dimensions\":\"\",\"Available_versions\":\"0\",\"ISBN_ELECTRONIC\":\"\",\"symbol_book\":\"\",\"price\":\"\",\"currency_id\":\"1\",\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_book\":\"hisham.jpg\",\"image_auther\":\"hisham.jpg\",\"image_translator\":\"hisham.jpg\",\"evaluation\":\"\",\"discount_price\":\"\",\"updated_at\":\"2020-06-19 12:08:46\",\"created_at\":\"2020-06-19 12:08:46\",\"id\":23}', '2020-06-19 19:08:46', '2020-06-19 19:08:46'),
(85, 1, 'system', 27, 'Category', 'Add new category ()', '', '{\"name_ar\":\"\",\"name_en\":\"\",\"date_of_publication\":\"\",\"ISBN\":\"\",\"lang_id\":\"1\",\"Edition\":\"\",\"original_publisher\":\"0\",\"cat_id\":\"2\",\"number_page\":\"\",\"dimensions\":\"\",\"Available_versions\":\"0\",\"ISBN_ELECTRONIC\":\"\",\"symbol_book\":\"\",\"price\":\"\",\"currency_id\":\"1\",\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_book\":\"hisham.jpg\",\"image_auther\":\"hisham.jpg\",\"image_translator\":\"hisham.jpg\",\"evaluation\":\"\",\"discount_price\":\"\",\"updated_at\":\"2020-06-19 12:15:49\",\"created_at\":\"2020-06-19 12:15:49\",\"id\":27}', '2020-06-19 19:15:49', '2020-06-19 19:15:49'),
(86, 1, 'system', 6, 'Panel', 'Add new panel (test)', '', '{\"name\":\"test\",\"updated_at\":\"2020-06-19 13:26:04\",\"created_at\":\"2020-06-19 13:26:04\",\"id\":6}', '2020-06-19 20:26:04', '2020-06-19 20:26:04'),
(87, 1, 'system', 5, 'Panels', 'Delete panel (event 2)', '{\"id\":5,\"name\":\"event 2\",\"updated_at\":\"-0001-11-30 00:00:00\",\"created_at\":\"-0001-11-30 00:00:00\"}', '', '2020-06-19 20:26:10', '2020-06-19 20:26:10'),
(88, 1, 'system', 6, 'Panels', 'Delete panel (test)', '{\"id\":6,\"name\":\"test\",\"updated_at\":\"2020-06-19 13:26:04\",\"created_at\":\"2020-06-19 13:26:04\"}', '', '2020-06-19 20:49:46', '2020-06-19 20:49:46'),
(89, 1, 'system', 8, 'Panel', 'Add new panel ()', '', '{\"image\":\"\",\"main_id\":\"2\",\"branch_id\":\"1\",\"professor\":\"iuyythtg\",\"title\":\"first type5t5t\",\"date\":\"2020-06-10\",\"updated_at\":\"2020-06-19 13:50:09\",\"created_at\":\"2020-06-19 13:50:09\",\"id\":8}', '2020-06-19 20:50:09', '2020-06-19 20:50:09'),
(90, 1, 'system', 9, 'Panel', 'Add new panel ()', '', '{\"image\":\"\",\"main_id\":\"2\",\"branch_id\":\"1\",\"professor\":\"3\",\"title\":\"first type5t5t\",\"date\":\"2020-06-09\",\"updated_at\":\"2020-06-19 14:00:26\",\"created_at\":\"2020-06-19 14:00:26\",\"id\":9}', '2020-06-19 21:00:26', '2020-06-19 21:00:26'),
(91, 1, 'system', 2, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"main_id\":\"2\",\"branch_id\":\"2\",\"title\":\"yubhnijmk,l.;\",\"date\":\"2020-06-10\",\"type_id\":\"1\",\"updated_at\":\"2020-06-20 14:38:06\",\"created_at\":\"2020-06-20 14:38:06\",\"id\":2}', '2020-06-20 21:38:07', '2020-06-20 21:38:07'),
(92, 1, 'system', 1, 'Panels', 'Delete panel ()', '{\"id\":1,\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"main_id\":3,\"branch_id\":1,\"type_id\":1,\"date\":\"2020-06-16\",\"title\":\"first kuykyukyukuyk\",\"updated_at\":\"2020-06-14 20:06:18\",\"created_at\":\"2020-06-14 20:06:18\"}', '', '2020-06-20 21:38:14', '2020-06-20 21:38:14'),
(93, 1, 'system', 10, 'Panel', 'Add new panel ()', '', '{\"image\":\"\",\"main_id\":\"2\",\"branch_id\":\"1\",\"professor\":\"3\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-18\",\"updated_at\":\"2020-06-20 15:12:02\",\"created_at\":\"2020-06-20 15:12:02\",\"id\":10}', '2020-06-20 22:12:02', '2020-06-20 22:12:02'),
(94, 1, 'system', 11, 'Panel', 'Add new panel ()', '', '{\"image\":\"\",\"main_id\":\"2\",\"branch_id\":\"1\",\"professor\":\"3\",\"title\":\"yubhnijmk,l.;\",\"date\":\"2020-06-20\",\"updated_at\":\"2020-06-20 15:13:24\",\"created_at\":\"2020-06-20 15:13:24\",\"id\":11}', '2020-06-20 22:13:24', '2020-06-20 22:13:24'),
(95, 1, 'system', 12, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"main_id\":\"2\",\"branch_id\":\"1\",\"professor\":\"3\",\"title\":\"first typeuu6uuu\",\"date\":\"2020-06-19\",\"updated_at\":\"2020-06-20 15:14:59\",\"created_at\":\"2020-06-20 15:14:59\",\"id\":12}', '2020-06-20 22:15:00', '2020-06-20 22:15:00'),
(96, 1, 'system', 4, 'Panel', 'Add new panel (هشام معاون)', '', '{\"name\":\"\\u0647\\u0634\\u0627\\u0645 \\u0645\\u0639\\u0627\\u0648\\u0646\",\"updated_at\":\"2020-06-20 15:29:32\",\"created_at\":\"2020-06-20 15:29:32\",\"id\":4}', '2020-06-20 22:29:32', '2020-06-20 22:29:32'),
(97, 1, 'system', 13, 'Panel', 'Add new panel ()', '', '{\"image\":\"\",\"main_id\":\"3\",\"branch_id\":\"2\",\"professor\":\"4\",\"title\":\"slider\",\"date\":\"2020-06-13\",\"updated_at\":\"2020-06-20 15:30:04\",\"created_at\":\"2020-06-20 15:30:04\",\"id\":13}', '2020-06-20 22:30:04', '2020-06-20 22:30:04'),
(98, 1, 'system', 11, 'Category', 'Delete catefry ()', '{\"id\":11,\"name_ar\":\"ewf\",\"name_en\":\"efwefewfefewfwee\",\"date_of_publication\":\"2020-06-08\",\"ISBN\":233,\"lang_id\":2,\"Edition\":0,\"original_publisher\":1,\"cat_id\":5,\"number_page\":323,\"dimensions\":3,\"Available_versions\":1,\"ISBN_ELECTRONIC\":233,\"ISBN_PRINTED\":0,\"symbol_book\":2332,\"price\":23323,\"currency_id\":2,\"about_book\":\"2ffrfwf\",\"about_auther\":\"efwefe\",\"about_translator\":\"\",\"image_book\":\"hisham.jpg\",\"image_auther\":\"hisham.jpg\",\"image_translator\":\"hisham.jpg\",\"evaluation\":3,\"discount_price\":1,\"updated_at\":\"2020-06-19 12:02:31\",\"created_at\":\"2020-06-19 12:02:31\"}', '', '2020-06-23 01:38:45', '2020-06-23 01:38:45'),
(99, 1, 'system', 2, 'Category', 'Edit category ()', '{\"id\":2,\"name_ar\":\"\\u062b\\u0642\\u064a\",\"name_en\":\"ytyyyyy\",\"date_of_publication\":\"2020-06-17\",\"ISBN\":232,\"lang_id\":2,\"Edition\":0,\"original_publisher\":1,\"cat_id\":4,\"number_page\":332,\"dimensions\":151551,\"Available_versions\":1,\"ISBN_ELECTRONIC\":32,\"ISBN_PRINTED\":0,\"symbol_book\":232,\"price\":3232,\"currency_id\":2,\"about_book\":\"fgeg\",\"about_auther\":\"ergerg\",\"about_translator\":\"ergr\",\"image_book\":\"\",\"image_auther\":\"\",\"image_translator\":\"\",\"evaluation\":2323,\"discount_price\":3232,\"updated_at\":\"2020-06-19 11:52:23\",\"created_at\":\"2020-06-19 11:29:14\"}', '{\"id\":2,\"name_ar\":\"\\u0643\\u062a\\u0627\\u0628 1\",\"name_en\":\"first book\",\"date_of_publication\":\"2020-06-17\",\"ISBN\":\"232\",\"lang_id\":\"2\",\"Edition\":\"0\",\"original_publisher\":\"1\",\"cat_id\":\"4\",\"number_page\":\"332\",\"dimensions\":\"151551\",\"Available_versions\":\"1\",\"ISBN_ELECTRONIC\":\"32\",\"ISBN_PRINTED\":0,\"symbol_book\":\"232\",\"price\":\"3232\",\"currency_id\":\"2\",\"about_book\":\"fgeg\",\"about_auther\":\"ergerg\",\"about_translator\":\"ergr\",\"image_book\":\"\",\"image_auther\":\"\",\"image_translator\":\"\",\"evaluation\":\"2323\",\"discount_price\":\"3232\",\"updated_at\":\"2020-06-22 18:40:25\",\"created_at\":\"2020-06-19 11:29:14\"}', '2020-06-23 01:40:25', '2020-06-23 01:40:25'),
(100, 1, 'system', 2, 'Category', 'Edit category ()', '{\"id\":2,\"name_ar\":\"\\u0643\\u062a\\u0627\\u0628 1\",\"name_en\":\"first book\",\"date_of_publication\":\"2020-06-17\",\"ISBN\":232,\"lang_id\":2,\"Edition\":0,\"original_publisher\":1,\"cat_id\":4,\"number_page\":332,\"dimensions\":151551,\"Available_versions\":1,\"ISBN_ELECTRONIC\":32,\"ISBN_PRINTED\":0,\"symbol_book\":232,\"price\":3232,\"currency_id\":2,\"about_book\":\"fgeg\",\"about_auther\":\"ergerg\",\"about_translator\":\"ergr\",\"image_book\":\"\",\"image_auther\":\"\",\"image_translator\":\"\",\"evaluation\":2323,\"discount_price\":3232,\"updated_at\":\"2020-06-22 18:40:25\",\"created_at\":\"2020-06-19 11:29:14\"}', '{\"id\":2,\"name_ar\":\"\\u0643\\u062a\\u0627\\u0628 1\",\"name_en\":\"first book\",\"date_of_publication\":\"2020-06-17\",\"ISBN\":\"232\",\"lang_id\":\"2\",\"Edition\":\"0\",\"original_publisher\":\"1\",\"cat_id\":\"4\",\"number_page\":\"332\",\"dimensions\":\"151551\",\"Available_versions\":\"1\",\"ISBN_ELECTRONIC\":\"32\",\"ISBN_PRINTED\":0,\"symbol_book\":\"232\",\"price\":\"3232\",\"currency_id\":\"2\",\"about_book\":\"fgeg\",\"about_auther\":\"ergerg\",\"about_translator\":\"ergr\",\"image_book\":\"\",\"image_auther\":\"\",\"image_translator\":\"\",\"evaluation\":\"2323\",\"discount_price\":\"3232\",\"updated_at\":\"2020-06-22 18:40:25\",\"created_at\":\"2020-06-19 11:29:14\"}', '2020-06-23 02:02:27', '2020-06-23 02:02:27'),
(101, 1, 'system', 28, 'Category', 'Add new category ()', '', '{\"name_ar\":\"\\u0643\\u062a\\u0627\\u0628 1\",\"name_en\":\"jj7j7\",\"date_of_publication\":\"2020-06-16\",\"ISBN\":\"232\",\"lang_id\":\"1\",\"Edition\":\"\\u0644\\u0641\\u0641\\u0644\\u0641\",\"original_publisher\":\"2\",\"cat_id\":\"3\",\"number_page\":\"22\",\"dimensions\":\"151551\",\"Available_versions\":\"1\",\"ISBN_ELECTRONIC\":\"2323\",\"symbol_book\":\"232\",\"price\":\"3232\",\"currency_id\":\"1\",\"about_book\":\"3edde\",\"about_auther\":\"ewdede\",\"about_translator\":\"dwede\",\"image_auther\":\"hisham.jpg\",\"image_translator\":\"hisham.jpg\",\"evaluation\":\"2323\",\"discount_price\":\"3232\",\"image_book\":\"1592852847.\",\"updated_at\":\"2020-06-22 19:07:27\",\"created_at\":\"2020-06-22 19:07:27\",\"id\":28}', '2020-06-23 02:07:28', '2020-06-23 02:07:28');
INSERT INTO `logs` (`id`, `user_id`, `user_type`, `item_id`, `item_type`, `message`, `old_value`, `new_value`, `created_at`, `updated_at`) VALUES
(102, 1, 'system', 29, 'Category', 'Add new category ()', '', '{\"name_ar\":\"\",\"name_en\":\"\",\"date_of_publication\":\"\",\"ISBN\":\"\",\"lang_id\":\"1\",\"Edition\":\"\",\"original_publisher\":\"0\",\"cat_id\":\"2\",\"number_page\":\"\",\"dimensions\":\"\",\"Available_versions\":\"0\",\"ISBN_ELECTRONIC\":\"\",\"symbol_book\":\"\",\"price\":\"\",\"currency_id\":\"1\",\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_auther\":\"hisham.jpg\",\"image_translator\":\"hisham.jpg\",\"evaluation\":\"\",\"discount_price\":\"\",\"image_book\":\"1592853111.\",\"updated_at\":\"2020-06-22 19:11:51\",\"created_at\":\"2020-06-22 19:11:51\",\"id\":29}', '2020-06-23 02:11:51', '2020-06-23 02:11:51'),
(103, 1, 'system', 30, 'Category', 'Add new category ()', '', '{\"name_ar\":\"\",\"name_en\":\"\",\"date_of_publication\":\"\",\"ISBN\":\"\",\"lang_id\":\"1\",\"Edition\":\"\",\"original_publisher\":\"0\",\"cat_id\":\"2\",\"number_page\":\"\",\"dimensions\":\"\",\"Available_versions\":\"0\",\"ISBN_ELECTRONIC\":\"\",\"symbol_book\":\"\",\"price\":\"\",\"currency_id\":\"1\",\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_auther\":{},\"image_translator\":{},\"evaluation\":\"\",\"discount_price\":\"\",\"updated_at\":\"2020-06-22 19:18:13\",\"created_at\":\"2020-06-22 19:18:13\",\"id\":30}', '2020-06-23 02:18:13', '2020-06-23 02:18:13'),
(104, 1, 'system', 31, 'Category', 'Add new category ()', '', '{\"name_ar\":\"\",\"name_en\":\"\",\"date_of_publication\":\"\",\"ISBN\":\"\",\"lang_id\":\"1\",\"Edition\":\"\",\"original_publisher\":\"0\",\"cat_id\":\"2\",\"number_page\":\"\",\"dimensions\":\"\",\"Available_versions\":\"0\",\"ISBN_ELECTRONIC\":\"\",\"symbol_book\":\"\",\"price\":\"\",\"currency_id\":\"1\",\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_auther\":{},\"image_translator\":{},\"evaluation\":\"\",\"discount_price\":\"\",\"updated_at\":\"2020-06-22 19:22:08\",\"created_at\":\"2020-06-22 19:22:08\",\"id\":31}', '2020-06-23 02:22:08', '2020-06-23 02:22:08'),
(105, 1, 'system', 32, 'Category', 'Add new category ()', '', '{\"name_ar\":\"\",\"name_en\":\"\",\"date_of_publication\":\"\",\"ISBN\":\"\",\"lang_id\":\"1\",\"Edition\":\"\",\"original_publisher\":\"0\",\"cat_id\":\"2\",\"number_page\":\"\",\"dimensions\":\"\",\"Available_versions\":\"0\",\"ISBN_ELECTRONIC\":\"\",\"symbol_book\":\"\",\"price\":\"\",\"currency_id\":\"1\",\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_auther\":{},\"image_translator\":{},\"evaluation\":\"\",\"discount_price\":\"\",\"updated_at\":\"2020-06-22 19:25:22\",\"created_at\":\"2020-06-22 19:25:22\",\"id\":32}', '2020-06-23 02:25:23', '2020-06-23 02:25:23'),
(106, 1, 'system', 33, 'Category', 'Add new category ()', '', '{\"name_ar\":\"\",\"name_en\":\"\",\"date_of_publication\":\"\",\"ISBN\":\"\",\"lang_id\":\"1\",\"Edition\":\"\",\"original_publisher\":\"0\",\"cat_id\":\"2\",\"number_page\":\"\",\"dimensions\":\"\",\"Available_versions\":\"0\",\"ISBN_ELECTRONIC\":\"\",\"symbol_book\":\"\",\"price\":\"\",\"currency_id\":\"1\",\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_auther\":{},\"image_translator\":{},\"evaluation\":\"\",\"discount_price\":\"\",\"updated_at\":\"2020-06-22 19:26:05\",\"created_at\":\"2020-06-22 19:26:05\",\"id\":33}', '2020-06-23 02:26:05', '2020-06-23 02:26:05'),
(107, 1, 'system', 34, 'Category', 'Add new category ()', '', '{\"name_ar\":\"\",\"name_en\":\"\",\"date_of_publication\":\"\",\"ISBN\":\"\",\"lang_id\":\"1\",\"Edition\":\"\",\"original_publisher\":\"0\",\"cat_id\":\"2\",\"number_page\":\"\",\"dimensions\":\"\",\"Available_versions\":\"0\",\"ISBN_ELECTRONIC\":\"\",\"symbol_book\":\"\",\"price\":\"\",\"currency_id\":\"1\",\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_auther\":{},\"image_translator\":{},\"evaluation\":\"\",\"discount_price\":\"\",\"updated_at\":\"2020-06-22 19:29:45\",\"created_at\":\"2020-06-22 19:29:45\",\"id\":34}', '2020-06-23 02:29:45', '2020-06-23 02:29:45'),
(108, 1, 'system', 35, 'Category', 'Add new category ()', '', '{\"name_ar\":\"\",\"name_en\":\"\",\"date_of_publication\":\"\",\"ISBN\":\"\",\"lang_id\":\"1\",\"Edition\":\"\",\"original_publisher\":\"0\",\"cat_id\":\"2\",\"number_page\":\"\",\"dimensions\":\"\",\"Available_versions\":\"0\",\"ISBN_ELECTRONIC\":\"\",\"symbol_book\":\"\",\"price\":\"\",\"currency_id\":\"1\",\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_auther\":{},\"image_translator\":{},\"evaluation\":\"\",\"discount_price\":\"\",\"updated_at\":\"2020-06-22 19:30:33\",\"created_at\":\"2020-06-22 19:30:33\",\"id\":35}', '2020-06-23 02:30:33', '2020-06-23 02:30:33'),
(109, 1, 'system', 36, 'Category', 'Add new category ()', '', '{\"name_ar\":\"\",\"name_en\":\"\",\"date_of_publication\":\"\",\"ISBN\":\"\",\"lang_id\":\"1\",\"Edition\":\"\",\"original_publisher\":\"0\",\"cat_id\":\"2\",\"number_page\":\"\",\"dimensions\":\"\",\"Available_versions\":\"0\",\"ISBN_ELECTRONIC\":\"\",\"symbol_book\":\"\",\"price\":\"\",\"currency_id\":\"1\",\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_auther\":{},\"image_translator\":{},\"evaluation\":\"\",\"discount_price\":\"\",\"image_book\":\"\\/asset\\/images\\/_1592854442.jpg\",\"updated_at\":\"2020-06-22 19:34:02\",\"created_at\":\"2020-06-22 19:34:02\",\"id\":36}', '2020-06-23 02:34:03', '2020-06-23 02:34:03'),
(110, 1, 'system', 37, 'Category', 'Add new category ()', '', '{\"name_ar\":\"\",\"name_en\":\"\",\"date_of_publication\":\"\",\"ISBN\":\"\",\"lang_id\":\"1\",\"Edition\":\"\",\"original_publisher\":\"0\",\"cat_id\":\"2\",\"number_page\":\"\",\"dimensions\":\"\",\"Available_versions\":\"0\",\"ISBN_ELECTRONIC\":\"\",\"symbol_book\":\"\",\"price\":\"\",\"currency_id\":\"1\",\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_auther\":{},\"image_translator\":{},\"evaluation\":\"\",\"discount_price\":\"\",\"image_book\":\"1592854957.jpg\",\"updated_at\":\"2020-06-22 19:42:37\",\"created_at\":\"2020-06-22 19:42:37\",\"id\":37}', '2020-06-23 02:42:37', '2020-06-23 02:42:37'),
(111, 1, 'system', 38, 'Category', 'Add new category ()', '', '{\"name_ar\":\"\",\"name_en\":\"\",\"date_of_publication\":\"\",\"ISBN\":\"\",\"lang_id\":\"1\",\"Edition\":\"\",\"original_publisher\":\"0\",\"cat_id\":\"2\",\"number_page\":\"\",\"dimensions\":\"\",\"Available_versions\":\"0\",\"ISBN_ELECTRONIC\":\"\",\"symbol_book\":\"\",\"price\":\"\",\"currency_id\":\"1\",\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_auther\":{},\"image_translator\":{},\"evaluation\":\"\",\"discount_price\":\"\",\"image_book\":\"E:\\\\projects\\\\FreeLance\\\\farnc\\\\Ecommerce\\\\project\\\\backend\\\\maganic\\\\public\\\\asset\\/images\\/jpg\",\"updated_at\":\"2020-06-22 19:44:38\",\"created_at\":\"2020-06-22 19:44:38\",\"id\":38}', '2020-06-23 02:44:39', '2020-06-23 02:44:39'),
(112, 1, 'system', 39, 'Category', 'Add new category ()', '', '{\"name_ar\":\"\",\"name_en\":\"\",\"date_of_publication\":\"\",\"ISBN\":\"\",\"lang_id\":\"1\",\"Edition\":\"\",\"original_publisher\":\"0\",\"cat_id\":\"2\",\"number_page\":\"\",\"dimensions\":\"\",\"Available_versions\":\"0\",\"ISBN_ELECTRONIC\":\"\",\"symbol_book\":\"\",\"price\":\"\",\"currency_id\":\"1\",\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_auther\":{},\"image_translator\":{},\"evaluation\":\"\",\"discount_price\":\"\",\"image_book\":\"1592855320.jpg\",\"updated_at\":\"2020-06-22 19:48:40\",\"created_at\":\"2020-06-22 19:48:40\",\"id\":39}', '2020-06-23 02:48:41', '2020-06-23 02:48:41'),
(113, 1, 'system', 44, 'Category', 'Add new category ()', '', '{\"name_ar\":\"\",\"name_en\":\"\",\"date_of_publication\":\"\",\"ISBN\":\"\",\"lang_id\":\"1\",\"Edition\":\"\",\"original_publisher\":\"0\",\"cat_id\":\"2\",\"number_page\":\"\",\"dimensions\":\"\",\"Available_versions\":\"0\",\"ISBN_ELECTRONIC\":\"\",\"symbol_book\":\"\",\"price\":\"\",\"currency_id\":\"1\",\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_auther\":{},\"image_translator\":{},\"evaluation\":\"\",\"discount_price\":\"\",\"image_book\":\"1592855896.jpg\",\"updated_at\":\"2020-06-22 19:58:16\",\"created_at\":\"2020-06-22 19:58:16\",\"id\":44}', '2020-06-23 02:58:17', '2020-06-23 02:58:17'),
(114, 1, 'system', 45, 'Category', 'Add new category ()', '', '{\"name_ar\":\"hiiiiiiii\",\"name_en\":\"\",\"date_of_publication\":\"\",\"ISBN\":\"\",\"lang_id\":\"1\",\"Edition\":\"\",\"original_publisher\":\"0\",\"cat_id\":\"2\",\"number_page\":\"\",\"dimensions\":\"\",\"Available_versions\":\"0\",\"ISBN_ELECTRONIC\":\"\",\"symbol_book\":\"\",\"price\":\"\",\"currency_id\":\"1\",\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_auther\":{},\"image_translator\":{},\"evaluation\":\"\",\"discount_price\":\"\",\"image_book\":\"1592856346.jpg\",\"updated_at\":\"2020-06-22 20:05:46\",\"created_at\":\"2020-06-22 20:05:46\",\"id\":45}', '2020-06-23 03:05:46', '2020-06-23 03:05:46'),
(115, 1, 'system', 1, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"main_id\":\"3\",\"branch_id\":\"1\",\"translator_id\":\"1\",\"auther_id\":\"1\",\"title\":\"slider\",\"date\":\"2020-06-26\",\"updated_at\":\"2020-06-23 08:05:41\",\"created_at\":\"2020-06-23 08:05:41\",\"id\":1}', '2020-06-23 15:05:41', '2020-06-23 15:05:41'),
(116, 1, 'system', 2, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"main_id\":\"3\",\"branch_id\":\"1\",\"translator_id\":\"1\",\"auther_id\":\"1\",\"title\":\"slider\",\"date\":\"2020-06-26\",\"updated_at\":\"2020-06-23 08:06:06\",\"created_at\":\"2020-06-23 08:06:06\",\"id\":2}', '2020-06-23 15:06:06', '2020-06-23 15:06:06'),
(117, 1, 'system', 3, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"main_id\":\"2\",\"branch_id\":\"2\",\"translator_id\":\"2\",\"auther_id\":\"1\",\"title\":\"first type5t5t\",\"date\":\"2020-06-16\",\"updated_at\":\"2020-06-23 08:26:24\",\"created_at\":\"2020-06-23 08:26:24\",\"id\":3}', '2020-06-23 15:26:24', '2020-06-23 15:26:24'),
(118, 1, 'system', 1, 'Panels', 'Delete panel ()', '{\"id\":1,\"main_id\":3,\"branch_id\":1,\"auther_id\":1,\"translator_id\":1,\"title\":\"slider\",\"date\":\"2020-06-26\",\"updated_at\":\"2020-06-23 08:05:41\",\"created_at\":\"2020-06-23 08:05:41\",\"image\":\"\\/uploads\\/files\\/vcredist.bmp\"}', '', '2020-06-23 15:26:30', '2020-06-23 15:26:30'),
(119, 1, 'system', 3, 'Panels', 'Delete panel ()', '{\"id\":3,\"main_id\":2,\"branch_id\":2,\"auther_id\":1,\"translator_id\":2,\"title\":\"first type5t5t\",\"date\":\"2020-06-16\",\"updated_at\":\"2020-06-23 08:26:24\",\"created_at\":\"2020-06-23 08:26:24\",\"image\":\"\\/uploads\\/files\\/vcredist.bmp\"}', '', '2020-06-23 15:29:11', '2020-06-23 15:29:11'),
(120, 1, 'system', 2, 'Panels', 'Delete panel ()', '{\"id\":2,\"main_id\":3,\"branch_id\":1,\"auther_id\":1,\"translator_id\":1,\"title\":\"slider\",\"date\":\"2020-06-26\",\"updated_at\":\"2020-06-23 08:06:06\",\"created_at\":\"2020-06-23 08:06:06\",\"image\":\"\\/uploads\\/files\\/vcredist.bmp\"}', '', '2020-06-23 15:29:43', '2020-06-23 15:29:43'),
(121, 1, 'system', 4, 'Panel', 'Add new panel ()', '', '{\"image\":\"\",\"main_id\":\"2\",\"branch_id\":\"1\",\"translator_id\":\"1\",\"auther_id\":\"2\",\"title\":\"first typeuu6uuu\",\"date\":\"2020-06-17\",\"updated_at\":\"2020-06-23 08:30:30\",\"created_at\":\"2020-06-23 08:30:30\",\"id\":4}', '2020-06-23 15:30:31', '2020-06-23 15:30:31'),
(122, 1, 'system', 7, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/12973483_1726075144272877_194223251258333372_o.jpg\",\"main_id\":\"3\",\"branch_id\":\"1\",\"translator_id\":\"1\",\"auther_id\":\"1\",\"title\":\"first type\",\"date\":\"2020-06-18\",\"updated_at\":\"2020-06-23 08:57:57\",\"created_at\":\"2020-06-23 08:57:57\",\"id\":7}', '2020-06-23 15:57:57', '2020-06-23 15:57:57'),
(123, 1, 'system', 7, 'Panels', 'Edit panel ()', '{\"id\":7,\"main_id\":3,\"branch_id\":1,\"auther_id\":1,\"translator_id\":1,\"title\":\"first type\",\"date\":\"2020-06-18\",\"updated_at\":\"2020-06-23 08:57:57\",\"created_at\":\"2020-06-23 08:57:57\",\"image\":\"\\/uploads\\/files\\/12973483_1726075144272877_194223251258333372_o.jpg\"}', '{\"id\":7,\"main_id\":\"3\",\"branch_id\":\"1\",\"auther_id\":\"1\",\"translator_id\":\"1\",\"title\":\"first typeuu6uuuukuku\",\"date\":\"2020-06-18\",\"updated_at\":\"2020-06-23 08:58:57\",\"created_at\":\"2020-06-23 08:57:57\",\"image\":\"\\/uploads\\/files\\/12973483_1726075144272877_194223251258333372_o.jpg\"}', '2020-06-23 15:58:58', '2020-06-23 15:58:58'),
(124, 1, 'system', 7, 'Panels', 'Edit panel ()', '{\"id\":7,\"main_id\":3,\"branch_id\":1,\"auther_id\":1,\"translator_id\":1,\"title\":\"first typeuu6uuuukuku\",\"date\":\"2020-06-18\",\"updated_at\":\"2020-06-23 08:58:57\",\"created_at\":\"2020-06-23 08:57:57\",\"image\":\"\\/uploads\\/files\\/12973483_1726075144272877_194223251258333372_o.jpg\"}', '{\"id\":7,\"main_id\":\"3\",\"branch_id\":\"1\",\"auther_id\":\"1\",\"translator_id\":\"1\",\"title\":\"first typeuu6uuuukuku\",\"date\":\"2020-06-18\",\"updated_at\":\"2020-06-23 08:58:57\",\"created_at\":\"2020-06-23 08:57:57\",\"image\":\"\\/uploads\\/files\\/12973483_1726075144272877_194223251258333372_o.jpg\"}', '2020-06-23 15:59:09', '2020-06-23 15:59:09'),
(125, 1, 'system', 0, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/Capture.png\",\"description\":\"trhtrhr h rthtrhrthrth h hrthtrhert thrthrrt\",\"updated_at\":\"2020-06-23 18:38:17\",\"created_at\":\"2020-06-23 18:38:17\",\"id\":0}', '2020-06-24 01:38:17', '2020-06-24 01:38:17'),
(126, 1, 'system', 0, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/Capture.png\",\"description\":\"tgtrhtrhrtehth\",\"updated_at\":\"2020-06-23 18:39:08\",\"created_at\":\"2020-06-23 18:39:08\",\"id\":0}', '2020-06-24 01:39:08', '2020-06-24 01:39:08'),
(127, 1, 'system', 6, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/Capture.png\",\"description\":\"thtr  jtrjytjytj yjtyjtyjytjjrt ytjtyrjy jyjyjyrj\",\"updated_at\":\"2020-06-23 18:40:12\",\"created_at\":\"2020-06-23 18:40:12\",\"id\":6}', '2020-06-24 01:40:13', '2020-06-24 01:40:13'),
(128, 1, 'system', 7, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/Capture.png\",\"description\":\"gregerg tgthtrh hththeth  htherthhth \",\"updated_at\":\"2020-06-23 18:40:37\",\"created_at\":\"2020-06-23 18:40:37\",\"id\":7}', '2020-06-24 01:40:37', '2020-06-24 01:40:37'),
(129, 1, 'system', 6, 'Panels', 'Delete panel ()', '{\"description\":\"thtr  jtrjytjytj yjtyjtyjytjjrt ytjtyrjy jyjyjyrj\",\"image\":\"\\/uploads\\/files\\/Capture.png\",\"created_at\":\"2020-06-23 18:40:12\",\"updated_at\":\"2020-06-23 18:40:12\",\"id\":6}', '', '2020-06-24 01:44:02', '2020-06-24 01:44:02'),
(130, 1, 'system', 7, 'Panels', 'Edit panel ()', '{\"description\":\"gregerg tgthtrh hththeth  htherthhth \",\"image\":\"\\/uploads\\/files\\/Capture.png\",\"created_at\":\"2020-06-23 18:40:37\",\"updated_at\":\"2020-06-23 18:40:37\",\"id\":7}', '{\"description\":null,\"image\":\"\\/uploads\\/files\\/Capture.png\",\"created_at\":\"2020-06-23 18:40:37\",\"updated_at\":\"2020-06-23 18:47:15\",\"id\":7}', '2020-06-24 01:47:15', '2020-06-24 01:47:15'),
(131, 1, 'system', 7, 'Panels', 'Edit panel ()', '{\"description\":\"\",\"image\":\"\\/uploads\\/files\\/Capture.png\",\"created_at\":\"2020-06-23 18:40:37\",\"updated_at\":\"2020-06-23 18:47:15\",\"id\":7}', '{\"description\":\"u6yhtrge\",\"image\":\"\\/uploads\\/files\\/Capture.png\",\"created_at\":\"2020-06-23 18:40:37\",\"updated_at\":\"2020-06-23 18:47:54\",\"id\":7}', '2020-06-24 01:47:54', '2020-06-24 01:47:54'),
(132, 1, 'system', 2, 'Panels', 'Edit panel ()', '{\"id\":2,\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"main_id\":2,\"branch_id\":2,\"type_id\":1,\"date\":\"2020-06-10\",\"title\":\"yubhnijmk,l.;\",\"updated_at\":\"2020-06-20 14:38:06\",\"created_at\":\"2020-06-20 14:38:06\",\"vedio\":null}', '{\"id\":2,\"image\":\"\\/uploads\\/files\\/Capture.png\",\"main_id\":\"2\",\"branch_id\":\"2\",\"type_id\":\"1\",\"date\":\"2020-06-10\",\"title\":\"yubhnijmk,l.;\",\"updated_at\":\"2020-06-23 21:47:58\",\"created_at\":\"2020-06-20 14:38:06\",\"vedio\":null}', '2020-06-24 04:47:58', '2020-06-24 04:47:58'),
(133, 1, 'system', 5, 'Panel', 'Add new panel (hisham)', '', '{\"name\":\"hisham\",\"lang_id\":\"1\",\"updated_at\":\"2020-06-24 19:13:28\",\"created_at\":\"2020-06-24 19:13:28\",\"id\":5}', '2020-06-25 02:13:28', '2020-06-25 02:13:28'),
(134, 1, 'system', 6, 'Panel', 'Add new panel (test)', '', '{\"name\":\"test\",\"lang_id\":\"2\",\"updated_at\":\"2020-06-24 19:13:36\",\"created_at\":\"2020-06-24 19:13:36\",\"id\":6}', '2020-06-25 02:13:36', '2020-06-25 02:13:36'),
(135, 1, 'system', 6, 'Panels', 'Edit panel (test)', '{\"id\":6,\"name\":\"test\",\"updated_at\":\"2020-06-24 19:13:36\",\"created_at\":\"2020-06-24 19:13:36\",\"lang_id\":2}', '{\"id\":6,\"name\":\"test\",\"updated_at\":\"2020-06-24 19:14:37\",\"created_at\":\"2020-06-24 19:13:36\",\"lang_id\":\"1\"}', '2020-06-25 02:14:37', '2020-06-25 02:14:37'),
(136, 1, 'system', 7, 'Panel', 'Add new panel ()', '', '{\"name\":\"\",\"lang_id\":\"1\",\"updated_at\":\"2020-06-24 19:15:21\",\"created_at\":\"2020-06-24 19:15:21\",\"id\":7}', '2020-06-25 02:15:21', '2020-06-25 02:15:21'),
(137, 1, 'system', 4, 'Panels', 'Delete panel (event 1)', '{\"id\":4,\"name\":\"event 1\",\"updated_at\":\"-0001-11-30 00:00:00\",\"created_at\":\"-0001-11-30 00:00:00\",\"lang_id\":0}', '', '2020-06-25 02:15:35', '2020-06-25 02:15:35'),
(138, 1, 'system', 3, 'Panels', 'Delete panel (event 2)', '{\"id\":3,\"name\":\"event 2\",\"updated_at\":\"-0001-11-30 00:00:00\",\"created_at\":\"-0001-11-30 00:00:00\",\"lang_id\":0}', '', '2020-06-25 02:15:38', '2020-06-25 02:15:38'),
(139, 1, 'system', 2, 'Panels', 'Delete panel (event 1)', '{\"id\":2,\"name\":\"event 1\",\"updated_at\":\"-0001-11-30 00:00:00\",\"created_at\":\"-0001-11-30 00:00:00\",\"lang_id\":0}', '', '2020-06-25 02:15:42', '2020-06-25 02:15:42'),
(140, 1, 'system', 5, 'Panels', 'Delete panel (hisham)', '{\"id\":5,\"name\":\"hisham\",\"updated_at\":\"2020-06-24 19:13:28\",\"created_at\":\"2020-06-24 19:13:28\",\"lang_id\":1}', '', '2020-06-25 02:15:48', '2020-06-25 02:15:48'),
(141, 1, 'system', 6, 'Panels', 'Delete panel (test)', '{\"id\":6,\"name\":\"test\",\"updated_at\":\"2020-06-24 19:14:37\",\"created_at\":\"2020-06-24 19:13:36\",\"lang_id\":1}', '', '2020-06-25 02:15:52', '2020-06-25 02:15:52'),
(142, 1, 'system', 7, 'Panels', 'Delete panel ()', '{\"id\":7,\"name\":\"\",\"updated_at\":\"2020-06-24 19:15:21\",\"created_at\":\"2020-06-24 19:15:21\",\"lang_id\":1}', '', '2020-06-25 02:15:58', '2020-06-25 02:15:58'),
(143, 1, 'system', 2, 'Panels', 'Delete panel (event 1)', '{\"id\":2,\"name\":\"event 1\",\"updated_at\":\"-0001-11-30 00:00:00\",\"created_at\":\"-0001-11-30 00:00:00\",\"lang_id\":0}', '', '2020-06-25 02:16:35', '2020-06-25 02:16:35'),
(144, 1, 'system', 8, 'Panel', 'Add new panel (hisham)', '', '{\"name\":\"hisham\",\"lang_id\":\"2\",\"updated_at\":\"2020-06-24 19:23:00\",\"created_at\":\"2020-06-24 19:23:00\",\"id\":8}', '2020-06-25 02:23:00', '2020-06-25 02:23:00'),
(145, 1, 'system', 9, 'Panel', 'Add new panel (هشام معاون)', '', '{\"name\":\"\\u0647\\u0634\\u0627\\u0645 \\u0645\\u0639\\u0627\\u0648\\u0646\",\"lang_id\":\"1\",\"updated_at\":\"2020-06-24 19:23:09\",\"created_at\":\"2020-06-24 19:23:09\",\"id\":9}', '2020-06-25 02:23:09', '2020-06-25 02:23:09'),
(146, 1, 'system', 9, 'Panels', 'Delete panel (هشام معاون)', '{\"id\":9,\"name\":\"\\u0647\\u0634\\u0627\\u0645 \\u0645\\u0639\\u0627\\u0648\\u0646\",\"updated_at\":\"2020-06-24 19:23:09\",\"created_at\":\"2020-06-24 19:23:09\",\"lang_id\":1}', '', '2020-06-25 02:23:13', '2020-06-25 02:23:13'),
(147, 1, 'system', 8, 'Panels', 'Delete panel (hisham)', '{\"id\":8,\"name\":\"hisham\",\"updated_at\":\"2020-06-24 19:23:00\",\"created_at\":\"2020-06-24 19:23:00\",\"lang_id\":2}', '', '2020-06-25 02:23:17', '2020-06-25 02:23:17'),
(148, 1, 'system', 14, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/Capture.png\",\"main_id\":\"10\",\"branch_id\":\"1\",\"professor\":\"3\",\"title\":\"first type\",\"date\":\"2020-06-10\",\"lang_id\":\"1\",\"updated_at\":\"2020-06-24 19:29:56\",\"created_at\":\"2020-06-24 19:29:56\",\"id\":14}', '2020-06-25 02:29:56', '2020-06-25 02:29:56'),
(149, 1, 'system', 15, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/Capture.png\",\"main_id\":\"10\",\"branch_id\":\"2\",\"professor\":\"3\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-26\",\"lang_id\":\"1\",\"updated_at\":\"2020-06-24 19:30:19\",\"created_at\":\"2020-06-24 19:30:19\",\"id\":15}', '2020-06-25 02:30:20', '2020-06-25 02:30:20'),
(150, 1, 'system', 13, 'Panels', 'Delete panel ()', '{\"id\":13,\"image\":\"\",\"main_id\":3,\"branch_id\":2,\"professor\":\"4\",\"title\":\"slider\",\"date\":\"2020-06-13\",\"updated_at\":\"2020-06-20 15:30:04\",\"created_at\":\"1970-01-01 00:33:40\",\"vedio\":null,\"lang_id\":0}', '', '2020-06-25 02:30:27', '2020-06-25 02:30:27'),
(151, 1, 'system', 12, 'Panels', 'Delete panel ()', '{\"id\":12,\"image\":\"\\/uploads\\/files\\/vcredist.b\",\"main_id\":2,\"branch_id\":1,\"professor\":\"3\",\"title\":\"first typeuu6uuu\",\"date\":\"2020-06-19\",\"updated_at\":\"2020-06-20 15:14:59\",\"created_at\":\"1970-01-01 00:33:40\",\"vedio\":null,\"lang_id\":0}', '', '2020-06-25 02:30:31', '2020-06-25 02:30:31'),
(152, 1, 'system', 15, 'Panels', 'Edit panel ()', '{\"id\":15,\"image\":\"\\/uploads\\/files\\/Capture.pn\",\"main_id\":10,\"branch_id\":2,\"professor\":\"3\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-26\",\"updated_at\":\"2020-06-24 19:30:19\",\"created_at\":\"1970-01-01 00:33:40\",\"vedio\":null,\"lang_id\":1}', '{\"id\":15,\"image\":\"\\/uploads\\/files\\/Capture.pn\",\"main_id\":\"10\",\"branch_id\":\"2\",\"professor\":\"2\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-26\",\"updated_at\":\"2020-06-24 19:31:25\",\"created_at\":\"1970-01-01 00:33:40\",\"vedio\":null,\"lang_id\":\"2\"}', '2020-06-25 02:31:25', '2020-06-25 02:31:25'),
(153, 1, 'system', 14, 'Panels', 'Edit panel ()', '{\"id\":14,\"image\":\"\\/uploads\\/files\\/Capture.pn\",\"main_id\":10,\"branch_id\":1,\"professor\":\"3\",\"title\":\"first type\",\"date\":\"2020-06-10\",\"updated_at\":\"2020-06-24 19:29:56\",\"created_at\":\"1970-01-01 00:33:40\",\"vedio\":null,\"lang_id\":1}', '{\"id\":14,\"image\":\"\\/uploads\\/files\\/Capture.pn\",\"main_id\":\"10\",\"branch_id\":\"1\",\"professor\":\"3\",\"title\":\"first typeuu6uuu\",\"date\":\"2020-06-10\",\"updated_at\":\"2020-06-24 19:32:41\",\"created_at\":\"1970-01-01 00:33:40\",\"vedio\":null,\"lang_id\":\"1\"}', '2020-06-25 02:32:41', '2020-06-25 02:32:41'),
(154, 1, 'system', 15, 'Panels', 'Edit panel ()', '{\"id\":15,\"image\":\"\\/uploads\\/files\\/Capture.pn\",\"main_id\":10,\"branch_id\":2,\"professor\":\"2\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-26\",\"updated_at\":\"2020-06-24 19:31:25\",\"created_at\":\"1970-01-01 00:33:40\",\"vedio\":null,\"lang_id\":2}', '{\"id\":15,\"image\":\"\\/uploads\\/files\\/Capture.pn\",\"main_id\":\"10\",\"branch_id\":\"2\",\"professor\":\"3\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-26\",\"updated_at\":\"2020-06-24 19:33:02\",\"created_at\":\"1970-01-01 00:33:40\",\"vedio\":null,\"lang_id\":\"2\"}', '2020-06-25 02:33:02', '2020-06-25 02:33:02'),
(155, 1, 'system', 9, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/Capture.png\",\"main_id\":\"10\",\"branch_id\":\"1\",\"translator_id\":\"1\",\"auther_id\":\"1\",\"title\":\"first type\",\"date\":\"2020-06-17\",\"lang_id\":\"1\",\"updated_at\":\"2020-06-24 19:36:27\",\"created_at\":\"2020-06-24 19:36:27\",\"id\":9}', '2020-06-25 02:36:27', '2020-06-25 02:36:27'),
(156, 1, 'system', 4, 'Panels', 'Delete panel ()', '{\"id\":4,\"main_id\":2,\"branch_id\":1,\"auther_id\":2,\"translator_id\":1,\"title\":\"first typeuu6uuu\",\"date\":\"2020-06-17\",\"updated_at\":\"2020-06-23 08:30:30\",\"created_at\":\"2020-06-23 08:30:30\",\"image\":\"\\/uploads\\/files\\/12973483_1726075144272877_194223251258333372_o.jpg\",\"description\":\"egegg\",\"lang_id\":0}', '', '2020-06-25 02:36:43', '2020-06-25 02:36:43'),
(157, 1, 'system', 5, 'Panels', 'Delete panel ()', '{\"id\":5,\"main_id\":2,\"branch_id\":1,\"auther_id\":1,\"translator_id\":2,\"title\":\"slider\",\"date\":\"2020-06-11\",\"updated_at\":\"2020-06-23 08:56:45\",\"created_at\":\"2020-06-23 08:56:45\",\"image\":\"\\/uploads\\/files\\/12973483_1726075144272877_194223251258333372_o.jpg\",\"description\":null,\"lang_id\":0}', '', '2020-06-25 02:36:47', '2020-06-25 02:36:47'),
(158, 1, 'system', 6, 'Panels', 'Delete panel ()', '{\"id\":6,\"main_id\":3,\"branch_id\":1,\"auther_id\":1,\"translator_id\":1,\"title\":\"first type\",\"date\":\"2020-06-18\",\"updated_at\":\"2020-06-23 08:57:46\",\"created_at\":\"2020-06-23 08:57:46\",\"image\":\"\\/uploads\\/files\\/12973483_1726075144272877_194223251258333372_o.jpg\",\"description\":null,\"lang_id\":0}', '', '2020-06-25 02:36:50', '2020-06-25 02:36:50'),
(159, 1, 'system', 7, 'Panels', 'Delete panel ()', '{\"id\":7,\"main_id\":3,\"branch_id\":1,\"auther_id\":1,\"translator_id\":1,\"title\":\"first typeuu6uuuukuku\",\"date\":\"2020-06-18\",\"updated_at\":\"2020-06-23 08:58:57\",\"created_at\":\"2020-06-23 08:57:57\",\"image\":\"\\/uploads\\/files\\/12973483_1726075144272877_194223251258333372_o.jpg\",\"description\":null,\"lang_id\":0}', '', '2020-06-25 02:36:54', '2020-06-25 02:36:54'),
(160, 1, 'system', 8, 'Panels', 'Delete panel ()', '{\"id\":8,\"main_id\":1,\"branch_id\":1,\"auther_id\":1,\"translator_id\":1,\"title\":\"yhtheyh\",\"date\":\"2020-06-03\",\"updated_at\":\"2019-05-07 12:33:56\",\"created_at\":\"2019-05-07 12:33:56\",\"image\":\"\\/uploads\\/files\\/12973483_1726075144272877_194223251258333372_o.jpg\",\"description\":\"lukyvffreferrfrefr\",\"lang_id\":0}', '', '2020-06-25 02:36:58', '2020-06-25 02:36:58'),
(161, 1, 'system', 9, 'Panels', 'Edit panel ()', '{\"id\":9,\"main_id\":10,\"branch_id\":1,\"auther_id\":1,\"translator_id\":1,\"title\":\"first type\",\"date\":\"2020-06-17\",\"updated_at\":\"2020-06-24 19:36:27\",\"created_at\":\"2020-06-24 19:36:27\",\"image\":\"\\/uploads\\/files\\/Capture.png\",\"description\":null,\"lang_id\":1}', '{\"id\":9,\"main_id\":\"10\",\"branch_id\":\"1\",\"auther_id\":\"1\",\"translator_id\":\"1\",\"title\":\"first type\",\"date\":\"2020-06-17\",\"updated_at\":\"2020-06-24 19:37:07\",\"created_at\":\"2020-06-24 19:36:27\",\"image\":\"\\/uploads\\/files\\/Capture.png\",\"description\":null,\"lang_id\":\"2\"}', '2020-06-25 02:37:07', '2020-06-25 02:37:07'),
(162, 1, 'system', 9, 'Panels', 'Edit panel ()', '{\"id\":9,\"main_id\":10,\"branch_id\":1,\"auther_id\":1,\"translator_id\":1,\"title\":\"first type\",\"date\":\"2020-06-17\",\"updated_at\":\"2020-06-24 19:37:07\",\"created_at\":\"2020-06-24 19:36:27\",\"image\":\"\\/uploads\\/files\\/Capture.png\",\"description\":null,\"lang_id\":2}', '{\"id\":9,\"main_id\":\"10\",\"branch_id\":\"1\",\"auther_id\":\"1\",\"translator_id\":\"1\",\"title\":\"first type\",\"date\":\"2020-06-17\",\"updated_at\":\"2020-06-24 19:37:07\",\"created_at\":\"2020-06-24 19:36:27\",\"image\":\"\\/uploads\\/files\\/Capture.png\",\"description\":null,\"lang_id\":\"2\"}', '2020-06-25 02:38:03', '2020-06-25 02:38:03'),
(163, 1, 'system', 8, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/Capture.png\",\"main_id\":\"10\",\"branch_id\":\"2\",\"title\":\"first type\",\"date\":\"2020-06-10\",\"type_id\":\"2\",\"lang_id\":\"2\",\"updated_at\":\"2020-06-24 19:43:04\",\"created_at\":\"2020-06-24 19:43:04\",\"id\":8}', '2020-06-25 02:43:04', '2020-06-25 02:43:04'),
(164, 1, 'system', 8, 'Panels', 'Edit panel ()', '{\"id\":8,\"image\":\"\\/uploads\\/files\\/Capture.png\",\"main_id\":10,\"branch_id\":2,\"type_id\":2,\"date\":\"2020-06-10\",\"title\":\"first type\",\"updated_at\":\"2020-06-24 19:43:04\",\"created_at\":\"2020-06-24 19:43:04\",\"vedio\":null,\"lang_id\":2}', '{\"id\":8,\"image\":\"\\/uploads\\/files\\/Capture.png\",\"main_id\":\"10\",\"branch_id\":\"2\",\"type_id\":\"2\",\"date\":\"2020-06-10\",\"title\":\"first type\",\"updated_at\":\"2020-06-24 19:43:15\",\"created_at\":\"2020-06-24 19:43:04\",\"vedio\":null,\"lang_id\":\"1\"}', '2020-06-25 02:43:15', '2020-06-25 02:43:15'),
(165, 1, 'system', 5, 'Panel', 'Add new panel (هشام معاون)', '', '{\"name\":\"\\u0647\\u0634\\u0627\\u0645 \\u0645\\u0639\\u0627\\u0648\\u0646\",\"lang_id\":\"1\",\"updated_at\":\"2020-06-24 19:46:06\",\"created_at\":\"2020-06-24 19:46:06\",\"id\":5}', '2020-06-25 02:46:06', '2020-06-25 02:46:06'),
(166, 1, 'system', 5, 'Panels', 'Edit panel (هشام معاون)', '{\"id\":5,\"name\":\"\\u0647\\u0634\\u0627\\u0645 \\u0645\\u0639\\u0627\\u0648\\u0646\",\"updated_at\":\"2020-06-24 19:46:06\",\"created_at\":\"2020-06-24 19:46:06\",\"lang_id\":1}', '{\"id\":5,\"name\":\"\\u0647\\u0634\\u0627\\u0645 \\u0645\\u0639\\u0627\\u0648\\u0646\",\"updated_at\":\"2020-06-24 19:46:14\",\"created_at\":\"2020-06-24 19:46:06\",\"lang_id\":\"2\"}', '2020-06-25 02:46:14', '2020-06-25 02:46:14'),
(167, 1, 'system', 5, 'Panels', 'Edit panel (هشام معاون)', '{\"id\":5,\"name\":\"\\u0647\\u0634\\u0627\\u0645 \\u0645\\u0639\\u0627\\u0648\\u0646\",\"updated_at\":\"2020-06-24 19:46:14\",\"created_at\":\"2020-06-24 19:46:06\",\"lang_id\":2}', '{\"id\":5,\"name\":\"\\u0647\\u0634\\u0627\\u0645 \\u0645\\u0639\\u0627\\u0648\\u0646\",\"updated_at\":\"2020-06-24 19:46:14\",\"created_at\":\"2020-06-24 19:46:06\",\"lang_id\":\"2\"}', '2020-06-25 02:46:26', '2020-06-25 02:46:26'),
(168, 1, 'system', 14, 'Prodcuts', 'Add new product ()', '', '{\"auther_id\":\"2\",\"translator_id\":\"1\",\"title\":\"yubhnijmk,l.;\",\"type_new\":\"1\",\"depatement_id\":\"1\",\"date_publish\":\"2020-06-24\",\"lang_id\":\"1\",\"updated_at\":\"2020-06-24 20:18:09\",\"created_at\":\"2020-06-24 20:18:09\",\"id\":14}', '2020-06-25 03:18:09', '2020-06-25 03:18:09'),
(169, 1, 'system', 14, 'Product', 'Edit product ()', '{\"id\":14,\"date_publish\":\"2020-06-24\",\"depatement_id\":1,\"auther_id\":2,\"translator_id\":1,\"title\":\"yubhnijmk,l.;\",\"type_new\":1,\"updated_at\":\"2020-06-24 20:18:09\",\"created_at\":\"2020-06-24 20:18:09\",\"description\":null,\"lang_id\":1}', '{\"id\":14,\"date_publish\":\"2020-06-24\",\"depatement_id\":\"1\",\"auther_id\":\"2\",\"translator_id\":\"1\",\"title\":\"thththththththt,l.;\",\"type_new\":\"1\",\"updated_at\":\"2020-06-24 20:23:40\",\"created_at\":\"2020-06-24 20:18:09\",\"description\":null,\"lang_id\":\"1\"}', '2020-06-25 03:23:40', '2020-06-25 03:23:40'),
(170, 1, 'system', 14, 'Product', 'Edit product ()', '{\"id\":14,\"date_publish\":\"2020-06-24\",\"depatement_id\":1,\"auther_id\":2,\"translator_id\":1,\"title\":\"thththththththt,l.;\",\"type_new\":1,\"updated_at\":\"2020-06-24 20:23:40\",\"created_at\":\"2020-06-24 20:18:09\",\"description\":null,\"lang_id\":1}', '{\"id\":14,\"date_publish\":\"2020-06-24\",\"depatement_id\":\"1\",\"auther_id\":\"2\",\"translator_id\":\"1\",\"title\":\"thththththththt,l.;\",\"type_new\":\"1\",\"updated_at\":\"2020-06-24 20:23:54\",\"created_at\":\"2020-06-24 20:18:09\",\"description\":null,\"lang_id\":\"2\"}', '2020-06-25 03:23:54', '2020-06-25 03:23:54'),
(171, 1, 'system', 11, 'Product', 'Edit product ()', '{\"id\":11,\"date_publish\":\"2020-06-01\",\"depatement_id\":1,\"auther_id\":1,\"translator_id\":1,\"title\":\"hhhh\",\"type_new\":2,\"updated_at\":\"2020-06-01 00:00:00\",\"created_at\":\"2020-06-15 00:00:00\",\"description\":\"\\tdescription\",\"lang_id\":0}', '{\"id\":11,\"date_publish\":\"2020-06-01\",\"depatement_id\":\"1\",\"auther_id\":\"1\",\"translator_id\":\"1\",\"title\":\"hhhh\",\"type_new\":\"2\",\"updated_at\":\"2020-06-24 20:24:29\",\"created_at\":\"2020-06-15 00:00:00\",\"description\":\"\\tdescription\",\"lang_id\":\"1\"}', '2020-06-25 03:24:29', '2020-06-25 03:24:29'),
(172, 1, 'system', 11, 'Product', 'Edit product ()', '{\"id\":11,\"date_publish\":\"2020-06-01\",\"depatement_id\":1,\"auther_id\":1,\"translator_id\":1,\"title\":\"hhhh\",\"type_new\":2,\"updated_at\":\"2020-06-24 20:24:29\",\"created_at\":\"2020-06-15 00:00:00\",\"description\":\"\\tdescription\",\"lang_id\":1}', '{\"id\":11,\"date_publish\":\"2020-06-01\",\"depatement_id\":\"1\",\"auther_id\":\"1\",\"translator_id\":\"1\",\"title\":\"hhhh\",\"type_new\":\"2\",\"updated_at\":\"2020-06-24 20:24:29\",\"created_at\":\"2020-06-15 00:00:00\",\"description\":\"\\tdescription\",\"lang_id\":\"1\"}', '2020-06-25 03:24:33', '2020-06-25 03:24:33'),
(173, 1, 'system', 2, 'Products', 'Delete product ()', '{\"id\":2,\"date_publish\":\"2020-06-04\",\"depatement_id\":2,\"auther_id\":1,\"translator_id\":1,\"title\":\"first type\",\"type_new\":1,\"updated_at\":\"2020-06-12 11:17:22\",\"created_at\":\"2020-06-12 11:17:22\",\"description\":\"Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printe\",\"lang_id\":0}', '', '2020-06-25 03:24:38', '2020-06-25 03:24:38'),
(174, 1, 'system', 14, 'Product', 'Edit product ()', '{\"id\":14,\"date_publish\":\"2020-06-24\",\"depatement_id\":1,\"auther_id\":2,\"translator_id\":1,\"title\":\"thththththththt,l.;\",\"type_new\":1,\"updated_at\":\"2020-06-24 20:23:54\",\"created_at\":\"2020-06-24 20:18:09\",\"description\":null,\"lang_id\":2}', '{\"id\":14,\"date_publish\":\"2020-06-24\",\"depatement_id\":\"1\",\"auther_id\":\"2\",\"translator_id\":\"1\",\"title\":\"thththththththt,l.;\",\"type_new\":\"1\",\"updated_at\":\"2020-06-24 20:25:09\",\"created_at\":\"2020-06-24 20:18:09\",\"description\":null,\"lang_id\":\"1\"}', '2020-06-25 03:25:09', '2020-06-25 03:25:09'),
(175, 1, 'system', 14, 'Product', 'Edit product ()', '{\"id\":14,\"date_publish\":\"2020-06-24\",\"depatement_id\":1,\"auther_id\":2,\"translator_id\":1,\"title\":\"thththththththt,l.;\",\"type_new\":1,\"updated_at\":\"2020-06-24 20:25:09\",\"created_at\":\"2020-06-24 20:18:09\",\"description\":null,\"lang_id\":1}', '{\"id\":14,\"date_publish\":\"2020-06-24\",\"depatement_id\":\"1\",\"auther_id\":\"2\",\"translator_id\":\"1\",\"title\":\"thththththththt,l.;\",\"type_new\":\"1\",\"updated_at\":\"2020-06-24 20:25:19\",\"created_at\":\"2020-06-24 20:18:09\",\"description\":null,\"lang_id\":\"2\"}', '2020-06-25 03:25:19', '2020-06-25 03:25:19'),
(176, 1, 'system', 16, 'Panel', 'Add new panel ()', '', '{\"image\":\"\",\"main_id\":\"10\",\"branch_id\":\"1\",\"professor\":\"3\",\"title\":\"slider\",\"date\":\"2020-06-13\",\"lang_id\":\"1\",\"updated_at\":\"2020-06-24 20:28:55\",\"created_at\":\"2020-06-24 20:28:55\",\"id\":16}', '2020-06-25 03:28:55', '2020-06-25 03:28:55'),
(177, 1, 'system', 15, 'Panels', 'Edit panel ()', '{\"id\":15,\"image\":\"\\/uploads\\/files\\/Capture.pn\",\"main_id\":10,\"branch_id\":2,\"professor\":\"3\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-26\",\"updated_at\":\"2020-06-24 19:33:02\",\"created_at\":\"1970-01-01 00:33:40\",\"vedio\":null,\"lang_id\":2}', '{\"id\":15,\"image\":\"\\/uploads\\/files\\/Capture.pn\",\"main_id\":\"10\",\"branch_id\":\"2\",\"professor\":\"3\",\"title\":\"first kuykyukyukuyk\",\"date\":\"2020-06-26\",\"updated_at\":\"2020-06-24 20:29:01\",\"created_at\":\"1970-01-01 00:33:40\",\"vedio\":null,\"lang_id\":\"1\"}', '2020-06-25 03:29:01', '2020-06-25 03:29:01'),
(178, 1, 'system', 7, 'Category', 'Add new category ()', '', '{\"lang_id\":\"2\",\"date_publication\":\"2020-06-09\",\"title\":\"first type\",\"description\":\"u6yhtrgertwreertwtrtrw\",\"updated_at\":\"2020-06-24 20:30:23\",\"created_at\":\"2020-06-24 20:30:23\",\"id\":7}', '2020-06-25 03:30:24', '2020-06-25 03:30:24'),
(179, 1, 'system', 3, 'Category', 'Edit category ()', '{\"id\":3,\"date_publication\":\"2020-06-11\",\"title\":\"first type\",\"description\":\"u6yhtrge\",\"updated_at\":\"2020-06-12 11:42:42\",\"created_at\":\"2020-06-12 11:42:42\",\"lang_id\":0}', '{\"id\":3,\"date_publication\":null,\"title\":\"first type\",\"description\":\"first type\",\"updated_at\":\"2020-06-24 20:30:34\",\"created_at\":\"2020-06-12 11:42:42\",\"lang_id\":\"2\"}', '2020-06-25 03:30:34', '2020-06-25 03:30:34'),
(180, 1, 'system', 43, 'Category', 'Delete catefry ()', '{\"id\":43,\"name_ar\":\"\",\"name_en\":\"\",\"date_of_publication\":\"0000-00-00\",\"ISBN\":0,\"lang_id\":1,\"Edition\":0,\"original_publisher\":0,\"cat_id\":2,\"number_page\":0,\"dimensions\":0,\"Available_versions\":0,\"ISBN_ELECTRONIC\":0,\"ISBN_PRINTED\":0,\"symbol_book\":0,\"price\":0,\"currency_id\":1,\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_book\":\"1592855775.jpg\",\"image_auther\":\"E:\\\\xampp\\\\tmp\\\\phpB4D8.tmp\",\"image_translator\":\"E:\\\\xampp\\\\tmp\\\\phpB4D9.tmp\",\"evaluation\":0,\"discount_price\":0,\"updated_at\":\"2020-06-22 19:56:15\",\"created_at\":\"2020-06-22 19:56:15\"}', '', '2020-06-25 03:30:50', '2020-06-25 03:30:50'),
(181, 1, 'system', 42, 'Category', 'Delete catefry ()', '{\"id\":42,\"name_ar\":\"\\u0643\\u062a\\u0627\\u0628 1\",\"name_en\":\"\",\"date_of_publication\":\"0000-00-00\",\"ISBN\":0,\"lang_id\":1,\"Edition\":0,\"original_publisher\":0,\"cat_id\":2,\"number_page\":0,\"dimensions\":0,\"Available_versions\":0,\"ISBN_ELECTRONIC\":0,\"ISBN_PRINTED\":0,\"symbol_book\":0,\"price\":0,\"currency_id\":1,\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_book\":\"1592855478.jpg\",\"image_auther\":\"E:\\\\xampp\\\\tmp\\\\php2C6F.tmp\",\"image_translator\":\"E:\\\\xampp\\\\tmp\\\\php2C70.tmp\",\"evaluation\":0,\"discount_price\":0,\"updated_at\":\"2020-06-22 19:51:18\",\"created_at\":\"2020-06-22 19:51:18\"}', '', '2020-06-25 03:30:54', '2020-06-25 03:30:54'),
(182, 1, 'system', 45, 'Category', 'Delete catefry ()', '{\"id\":45,\"name_ar\":\"hiiiiiiii\",\"name_en\":\"\",\"date_of_publication\":\"0000-00-00\",\"ISBN\":0,\"lang_id\":1,\"Edition\":0,\"original_publisher\":0,\"cat_id\":2,\"number_page\":0,\"dimensions\":0,\"Available_versions\":0,\"ISBN_ELECTRONIC\":0,\"ISBN_PRINTED\":0,\"symbol_book\":0,\"price\":0,\"currency_id\":1,\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_book\":\"1592856346.jpg\",\"image_auther\":\"E:\\\\xampp\\\\tmp\\\\php6897.tmp\",\"image_translator\":\"E:\\\\xampp\\\\tmp\\\\php6898.tmp\",\"evaluation\":0,\"discount_price\":0,\"updated_at\":\"2020-06-22 20:05:46\",\"created_at\":\"2020-06-22 20:05:46\"}', '', '2020-06-25 03:30:58', '2020-06-25 03:30:58'),
(183, 1, 'system', 44, 'Category', 'Delete catefry ()', '{\"id\":44,\"name_ar\":\"\",\"name_en\":\"\",\"date_of_publication\":\"0000-00-00\",\"ISBN\":0,\"lang_id\":1,\"Edition\":0,\"original_publisher\":0,\"cat_id\":2,\"number_page\":0,\"dimensions\":0,\"Available_versions\":0,\"ISBN_ELECTRONIC\":0,\"ISBN_PRINTED\":0,\"symbol_book\":0,\"price\":0,\"currency_id\":1,\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_book\":\"1592855896.jpg\",\"image_auther\":\"E:\\\\xampp\\\\tmp\\\\php8D54.tmp\",\"image_translator\":\"E:\\\\xampp\\\\tmp\\\\php8D65.tmp\",\"evaluation\":0,\"discount_price\":0,\"updated_at\":\"2020-06-22 19:58:16\",\"created_at\":\"2020-06-22 19:58:16\"}', '', '2020-06-25 03:31:02', '2020-06-25 03:31:02'),
(184, 1, 'system', 41, 'Category', 'Edit category ()', '{\"id\":41,\"name_ar\":\"\\u0643\\u062a\\u0627\\u0628 1\",\"name_en\":\"\",\"date_of_publication\":\"0000-00-00\",\"ISBN\":0,\"lang_id\":1,\"Edition\":0,\"original_publisher\":0,\"cat_id\":2,\"number_page\":0,\"dimensions\":0,\"Available_versions\":0,\"ISBN_ELECTRONIC\":0,\"ISBN_PRINTED\":0,\"symbol_book\":0,\"price\":0,\"currency_id\":1,\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_book\":\"1592855457.jpg\",\"image_auther\":\"E:\\\\xampp\\\\tmp\\\\phpD88F.tmp\",\"image_translator\":\"E:\\\\xampp\\\\tmp\\\\phpD890.tmp\",\"evaluation\":0,\"discount_price\":0,\"updated_at\":\"2020-06-22 19:50:57\",\"created_at\":\"2020-06-22 19:50:57\"}', '{\"id\":41,\"name_ar\":\"\\u0643\\u062a\\u0627\\u0628 1\",\"name_en\":\"\",\"date_of_publication\":\"\",\"ISBN\":\"0\",\"lang_id\":\"2\",\"Edition\":\"0\",\"original_publisher\":\"1\",\"cat_id\":\"2\",\"number_page\":\"0\",\"dimensions\":\"0\",\"Available_versions\":\"0\",\"ISBN_ELECTRONIC\":\"0\",\"ISBN_PRINTED\":0,\"symbol_book\":\"0\",\"price\":\"0\",\"currency_id\":\"1\",\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_book\":\"hisham.jpg\",\"image_auther\":\"hisham.jpg\",\"image_translator\":\"hisham.jpg\",\"evaluation\":\"0\",\"discount_price\":\"0\",\"updated_at\":\"2020-06-24 20:31:46\",\"created_at\":\"2020-06-22 19:50:57\"}', '2020-06-25 03:31:47', '2020-06-25 03:31:47'),
(185, 1, 'system', 41, 'Category', 'Edit category ()', '{\"id\":41,\"name_ar\":\"\\u0643\\u062a\\u0627\\u0628 1\",\"name_en\":\"\",\"date_of_publication\":\"0000-00-00\",\"ISBN\":0,\"lang_id\":2,\"Edition\":0,\"original_publisher\":1,\"cat_id\":2,\"number_page\":0,\"dimensions\":0,\"Available_versions\":0,\"ISBN_ELECTRONIC\":0,\"ISBN_PRINTED\":0,\"symbol_book\":0,\"price\":0,\"currency_id\":1,\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_book\":\"hisham.jpg\",\"image_auther\":\"hisham.jpg\",\"image_translator\":\"hisham.jpg\",\"evaluation\":0,\"discount_price\":0,\"updated_at\":\"2020-06-24 20:31:46\",\"created_at\":\"2020-06-22 19:50:57\"}', '{\"id\":41,\"name_ar\":\"\\u0643\\u062a\\u0627\\u0628 1\",\"name_en\":\"\",\"date_of_publication\":\"\",\"ISBN\":\"0\",\"lang_id\":\"2\",\"Edition\":\"0\",\"original_publisher\":\"1\",\"cat_id\":\"2\",\"number_page\":\"0\",\"dimensions\":\"0\",\"Available_versions\":\"0\",\"ISBN_ELECTRONIC\":\"0\",\"ISBN_PRINTED\":0,\"symbol_book\":\"0\",\"price\":\"0\",\"currency_id\":\"1\",\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_book\":\"hisham.jpg\",\"image_auther\":\"hisham.jpg\",\"image_translator\":\"hisham.jpg\",\"evaluation\":\"0\",\"discount_price\":\"0\",\"updated_at\":\"2020-06-24 20:32:16\",\"created_at\":\"2020-06-22 19:50:57\"}', '2020-06-25 03:32:16', '2020-06-25 03:32:16'),
(186, 1, 'system', 41, 'Category', 'Edit category ()', '{\"id\":41,\"name_ar\":\"\\u0643\\u062a\\u0627\\u0628 1\",\"name_en\":\"\",\"date_of_publication\":\"0000-00-00\",\"ISBN\":0,\"lang_id\":2,\"Edition\":0,\"original_publisher\":1,\"cat_id\":2,\"number_page\":0,\"dimensions\":0,\"Available_versions\":0,\"ISBN_ELECTRONIC\":0,\"ISBN_PRINTED\":0,\"symbol_book\":0,\"price\":0,\"currency_id\":1,\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_book\":\"hisham.jpg\",\"image_auther\":\"hisham.jpg\",\"image_translator\":\"hisham.jpg\",\"evaluation\":0,\"discount_price\":0,\"updated_at\":\"2020-06-24 20:32:16\",\"created_at\":\"2020-06-22 19:50:57\"}', '{\"id\":41,\"name_ar\":\"\\u0643\\u062a\\u0627\\u0628 1\",\"name_en\":\"\",\"date_of_publication\":\"\",\"ISBN\":\"0\",\"lang_id\":\"1\",\"Edition\":\"0\",\"original_publisher\":\"1\",\"cat_id\":\"2\",\"number_page\":\"0\",\"dimensions\":\"0\",\"Available_versions\":\"0\",\"ISBN_ELECTRONIC\":\"0\",\"ISBN_PRINTED\":0,\"symbol_book\":\"0\",\"price\":\"0\",\"currency_id\":\"1\",\"about_book\":\"\",\"about_auther\":\"\",\"about_translator\":\"\",\"image_book\":\"hisham.jpg\",\"image_auther\":\"hisham.jpg\",\"image_translator\":\"hisham.jpg\",\"evaluation\":\"0\",\"discount_price\":\"0\",\"updated_at\":\"2020-06-24 20:32:43\",\"created_at\":\"2020-06-22 19:50:57\"}', '2020-06-25 03:32:43', '2020-06-25 03:32:43'),
(187, 1, 'system', 3, 'Category', 'Add new category ()', '', '{\"number\":\"54545\",\"image\":\"\\/uploads\\/files\\/vcredist.bmp\",\"pdf\":\"fist.docx\",\"updated_at\":\"2020-06-24 20:35:12\",\"created_at\":\"2020-06-24 20:35:12\",\"id\":3}', '2020-06-25 03:35:12', '2020-06-25 03:35:12'),
(188, 1, 'system', 8, 'Panel', 'Add new panel ()', '', '{\"image\":\"\\/uploads\\/files\\/Capture_1.png\",\"description\":\"hthtrhtherhtheht\",\"lang_id\":\"2\",\"updated_at\":\"2020-06-24 20:38:37\",\"created_at\":\"2020-06-24 20:38:37\",\"id\":8}', '2020-06-25 03:38:38', '2020-06-25 03:38:38'),
(189, 1, 'system', 8, 'Panels', 'Edit panel ()', '{\"description\":\"hthtrhtherhtheht\",\"image\":\"\\/uploads\\/files\\/Capture_1.png\",\"created_at\":\"2020-06-24 20:38:37\",\"updated_at\":\"2020-06-24 20:38:37\",\"id\":8,\"lang_id\":2}', '{\"description\":\"hthtrhtherhtheht\",\"image\":\"\\/uploads\\/files\\/Capture_1.png\",\"created_at\":\"2020-06-24 20:38:37\",\"updated_at\":\"2020-06-24 20:38:45\",\"id\":8,\"lang_id\":\"1\"}', '2020-06-25 03:38:45', '2020-06-25 03:38:45'),
(190, 1, 'system', 4, 'Category', 'Add new category ()', '', '{\"number\":\"\",\"image\":\"\\/uploads\\/files\\/Capture_1.png\",\"pdf\":\"1593032209.docx\",\"updated_at\":\"2020-06-24 20:56:49\",\"created_at\":\"2020-06-24 20:56:49\",\"id\":4}', '2020-06-25 03:56:49', '2020-06-25 03:56:49'),
(191, 1, 'system', 5, 'Category', 'Edit category ()', '{\"id\":4,\"number\":0,\"image\":\"\\/uploads\\/files\\/Capture_1.png\",\"updated_at\":\"2020-06-24 20:56:49\",\"created_at\":\"2020-06-24 20:56:49\",\"pdf\":\"1593032209.docx\"}', '{\"number\":\"0\",\"image\":\"\\/uploads\\/files\\/Capture_1.png\",\"pdf\":\"\",\"updated_at\":\"2020-06-24 21:19:38\",\"created_at\":\"2020-06-24 21:19:38\",\"id\":5}', '2020-06-25 04:19:38', '2020-06-25 04:19:38');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `date_publication` date NOT NULL,
  `title` varchar(25) NOT NULL,
  `description` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `date_publication`, `title`, `description`, `updated_at`, `created_at`, `lang_id`) VALUES
(3, '0000-00-00', 'first type', 'first type', '2020-06-24 20:30:34', '2020-06-12 11:42:42', 2),
(4, '2020-06-01', 'ffggfgdfgfgdfgf', 'fgdfgfdfgdfgdfgfdgfdgdfgfdgfdgfdgdfgfdgfdgfdgdfg', NULL, '0000-00-00 00:00:00', 0),
(6, '2020-06-01', 'IT', 'gergrgeg', '2020-06-22 00:00:00', '2019-05-16 10:55:06', 0),
(7, '2020-06-09', 'first type', 'u6yhtrgertwreertwtrtrw', '2020-06-24 20:30:23', '2020-06-24 20:30:23', 2);

-- --------------------------------------------------------

--
-- Table structure for table `newsletter`
--

CREATE TABLE `newsletter` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `newsletter`
--

INSERT INTO `newsletter` (`id`, `email`, `created_at`, `updated_at`) VALUES
(1, 'iskandar.salama@gmail.com', '2017-04-24 03:43:04', '2017-04-24 03:43:04');

-- --------------------------------------------------------

--
-- Table structure for table `number_count`
--

CREATE TABLE `number_count` (
  `id` int(11) NOT NULL,
  `number` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `pdf` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `number_count`
--

INSERT INTO `number_count` (`id`, `number`, `image`, `updated_at`, `created_at`, `pdf`) VALUES
(2, 33, '/uploads/files/12973483_1726075144272877_194223251258333372_o.jpg', '2020-06-15 19:03:20', '2020-06-15 19:03:20', ''),
(3, 54545, '/uploads/files/vcredist.bmp', '2020-06-24 20:35:12', '2020-06-24 20:35:12', ''),
(4, 0, '/uploads/files/Capture_1.png', '2020-06-24 20:56:49', '2020-06-24 20:56:49', '1593032209.docx'),
(5, 0, '/uploads/files/Capture_1.png', '2020-06-24 21:19:38', '2020-06-24 21:19:38', '');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `heading` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `content` text,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `parent_id`, `name`, `url`, `heading`, `photo`, `content`, `meta_title`, `meta_description`, `status`, `sort_order`, `created_at`, `updated_at`) VALUES
(1, 0, 'About Us', 'about-us', 'About Us', '', '<p>test test test</p>\r\n', 'About Us', 'About Us', 1, 1, '2017-04-12 10:36:56', '2017-04-12 10:36:56'),
(2, 0, 'Contacts', 'contacts', 'Contacts', '', '', '', '', 1, 2, '2017-04-12 10:37:16', '2017-04-12 10:37:16'),
(3, 1, 'How Are?', 'about-us/how-are', 'How Are?', '', '', '', '', 1, 1, '2017-04-12 10:54:40', '2017-06-12 07:17:35'),
(4, 1, 'Why Us?', 'about-us/why-us', 'Why Us?', '', '', '', '', 1, 2, '2017-04-12 10:55:07', '2017-04-12 10:55:07');

-- --------------------------------------------------------

--
-- Table structure for table `panels`
--

CREATE TABLE `panels` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `ident` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `code` text,
  `photo` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `sort_order` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `panels`
--

INSERT INTO `panels` (`id`, `group_id`, `name`, `ident`, `title`, `content`, `code`, `photo`, `url`, `status`, `sort_order`, `created_at`, `updated_at`) VALUES
(1, 1, 'Banner 1', '', '', '', '', '/uploads/files/banner.jpg', '', 1, 1, '2017-03-23 11:07:50', '2020-06-02 04:39:38'),
(2, 1, 'Banner 2', '', '', '', '', '/uploads/files/down.load.jpg', '', 0, 1, '2017-03-23 11:08:03', '2020-06-02 04:39:20'),
(3, 0, '', '', 'first kuykyukyukuyk', NULL, NULL, NULL, NULL, 0, 0, '2017-03-23 11:08:21', '2020-06-13 15:31:36'),
(4, 2, 'About', '', 'About', '<p>Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', '', '', '', 1, 1, '2017-03-29 07:43:33', '2017-03-29 07:43:33'),
(5, 3, 'Contacts', '', 'Contacts', '<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English.</p>\r\n', '', '', '', 1, 1, '2017-03-29 07:44:17', '2020-06-02 04:39:31'),
(6, 4, 'Newsletter', '', 'Newsletter', '<p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&#39;t look even slightly believable.</p>\r\n', '', '', '', 1, 1, '2017-03-29 07:44:40', '2017-03-29 07:46:28'),
(7, 5, 'Home Text', '', '', '<h1>Welcome To</h1>\r\n\r\n<p>Lorem Ipsum&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', '', '', '', 1, 1, '2017-04-12 11:23:48', '2020-06-02 04:39:35');

-- --------------------------------------------------------

--
-- Table structure for table `panels_groups`
--

CREATE TABLE `panels_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ident` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `panels_groups`
--

INSERT INTO `panels_groups` (`id`, `name`, `ident`, `created_at`, `updated_at`) VALUES
(1, 'Home Banners', 'banners', '2017-03-23 11:06:45', '2017-03-23 11:06:45'),
(2, 'About', 'about', '2017-03-29 07:42:36', '2017-03-29 07:42:36'),
(3, 'Contacts', 'contacts', '2017-03-29 07:42:44', '2017-03-29 07:42:44'),
(4, 'Newsletter', 'newsletter', '2017-03-29 07:42:51', '2017-03-29 07:42:51'),
(5, 'Home Text', 'home-text', '2017-04-12 11:22:24', '2017-04-12 11:22:24');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(10) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `parent_id`, `name`, `description`, `order`) VALUES
(65, 67, 'Edit User', '', 2),
(66, 67, 'Delete User', '', 3),
(67, 0, 'Users', 'Manage all system users', 1),
(68, 67, 'Add User', '', 1),
(70, 67, 'Change Password', 'Change user password by admin user', 4),
(72, 0, 'Roles', 'manage all system roles', 2),
(73, 72, 'Add Role', '', 1),
(74, 72, 'Edit Role', '', 2),
(75, 72, 'Delete Role', '', 3),
(76, 0, 'Permissions', '', 3),
(77, 76, 'Add Permission', '', 1),
(78, 76, 'Edit Permission', '', 2),
(79, 76, 'Delete Permission', '', 3),
(80, 0, 'Settings', '', 4),
(82, 0, 'Pages', 'manage all site pages', 4),
(83, 82, 'Add Page', '', 1),
(84, 82, 'Edit Page', '', 2),
(85, 82, 'Delete Page', '', 3),
(86, 82, 'Page Status', '', 4),
(87, 82, 'Page Table Actions', '', 5),
(88, 0, 'Panels', 'Manage all site panels', 5),
(89, 88, 'Add Panel', '', 1),
(90, 88, 'Edit Panel', '', 2),
(91, 88, 'Delete Panel', '', 3),
(92, 88, 'Panel Status', '', 4),
(93, 88, 'Panels Table Actions', '', 5),
(94, 0, 'Panel Groups', '', 6),
(95, 94, 'Add Panel Group', '', 1),
(96, 94, 'Edit Panel Group', '', 2),
(97, 94, 'Delete Group', '', 3),
(98, 0, 'Privileges', '', 7),
(99, 0, 'Logs', '', 8),
(107, 99, 'User Logs', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `permissions_slugs`
--

CREATE TABLE `permissions_slugs` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `permission_slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `permissions_slugs`
--

INSERT INTO `permissions_slugs` (`id`, `permission_id`, `permission_slug`) VALUES
(139, 67, 'users'),
(140, 68, 'users/create'),
(141, 68, 'users/store'),
(142, 65, 'users/{user}/edit'),
(143, 65, 'users/{user}'),
(144, 66, 'users/delete/{user}'),
(145, 70, 'users/change-password/{user}'),
(146, 72, 'roles'),
(147, 73, 'roles/create'),
(148, 73, 'roles/store'),
(149, 74, 'roles/{role}/edit'),
(150, 74, 'roles/{role}'),
(151, 75, 'roles/delete/{role}'),
(152, 76, 'permissions'),
(153, 77, 'permissions/create'),
(154, 77, 'permissions/store'),
(155, 78, 'permissions/{permission}/edit'),
(156, 78, 'permissions/{permission}'),
(157, 79, 'permissions/{permission}/delete'),
(158, 80, 'settings'),
(171, 82, 'pages'),
(172, 83, 'pages/create'),
(173, 83, 'pages/store'),
(174, 84, 'pages/{page}/edit'),
(175, 84, 'pages/{page}'),
(176, 85, 'pages/delete/{page}'),
(177, 86, 'pages/status/{page}/{status}'),
(178, 87, 'pages/save-table'),
(179, 88, 'panels'),
(180, 89, 'panels/create'),
(181, 89, 'panels/store'),
(182, 90, 'panels/{panel}/edit'),
(183, 90, 'panels/{panel}'),
(184, 91, 'panels/delete/{panel}'),
(185, 92, 'panels/status/{panel}/{status}'),
(186, 93, 'panels/save-table'),
(187, 94, 'panels-groups'),
(188, 95, 'panels-groups/create'),
(189, 95, 'panels-groups/store'),
(191, 96, 'panels-groups/{group}/edit'),
(192, 96, 'panels-groups/{group}'),
(193, 97, 'panels-groups/delete/{group}'),
(194, 98, 'privileges-configurations'),
(205, 99, 'logs'),
(206, 107, 'logs/users');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`id`, `permission_id`, `role_id`) VALUES
(7, 67, 6),
(8, 68, 6),
(9, 65, 6),
(10, 82, 6),
(11, 83, 6),
(12, 84, 6);

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `main_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `auther_id` int(11) NOT NULL,
  `translator_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `image` varchar(100) NOT NULL,
  `description` text,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `main_id`, `branch_id`, `auther_id`, `translator_id`, `title`, `date`, `updated_at`, `created_at`, `image`, `description`, `lang_id`) VALUES
(9, 10, 1, 1, 1, 'first type', '2020-06-17', '2020-06-24 19:37:07', '2020-06-24 19:36:27', '/uploads/files/Capture.png', NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 NOT NULL,
  `url` varchar(100) DEFAULT NULL,
  `image` varchar(200) DEFAULT NULL,
  `brief` varchar(200) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `price_discount` double DEFAULT NULL,
  `colors` int(11) DEFAULT NULL,
  `models` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name_en` varchar(50) DEFAULT NULL,
  `origin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `cat_id`, `name`, `url`, `image`, `brief`, `description`, `price`, `price_discount`, `colors`, `models`, `status`, `sort_order`, `created_at`, `updated_at`, `name_en`, `origin`) VALUES
(1, 5, 'prod1', 'prod1', '/uploads/files/knife.jpg', 'prod1', '<p>ppppf444 sdadas das</p>', 8, 4, NULL, NULL, 0, 1, '2017-06-12 13:08:08', '2017-07-05 07:32:45', NULL, 0),
(3, 2, 'ttt', '', '', '', '', NULL, NULL, NULL, NULL, 0, 1, '2017-06-12 13:23:07', '2017-06-12 13:23:07', NULL, 0),
(12, 2, 'هشام معاون', '7i57i', '/uploads/files/12973483_1726075144272877_194223251258333372_o.jpg', 'etqg', '<p>thth</p>', 1, 1, NULL, NULL, 1, 1, '2020-06-03 03:26:03', '2020-06-03 03:27:35', '', 4);

-- --------------------------------------------------------

--
-- Table structure for table `release`
--

CREATE TABLE `release` (
  `id` int(11) NOT NULL,
  `image` varchar(25) NOT NULL,
  `auther_id` int(11) NOT NULL,
  `title` varchar(25) NOT NULL,
  `translator_id` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `release`
--

INSERT INTO `release` (`id`, `image`, `auther_id`, `title`, `translator_id`, `updated_at`, `created_at`) VALUES
(1, '/uploads/files/vcredist.b', 1, 'first type', 1, '2020-06-14 19:09:27', '2020-06-14 19:09:27');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'Administrator'),
(6, 'Editor');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `ident` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `name`, `ident`, `value`, `created_at`, `updated_at`) VALUES
(1, 'System Name', 'system_name', 'Maganic Dashboard', '2017-01-18 08:14:31', '2020-06-19 20:43:14'),
(2, 'Meta Title', 'meta_title', 'maganic', '2017-01-18 08:21:00', '2020-06-19 20:43:14'),
(3, 'Reservations Emails', 'reservations_emails', 'moawenhisham@gmail.com', '2017-05-30 04:08:14', '2020-06-19 20:43:14');

-- --------------------------------------------------------

--
-- Table structure for table `studies`
--

CREATE TABLE `studies` (
  `id` int(11) NOT NULL,
  `date_publish` date NOT NULL,
  `depatement_id` int(11) NOT NULL,
  `auther_id` int(11) NOT NULL,
  `translator_id` int(11) NOT NULL,
  `title` varchar(25) NOT NULL,
  `type_new` int(11) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `description` text,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `studies`
--

INSERT INTO `studies` (`id`, `date_publish`, `depatement_id`, `auther_id`, `translator_id`, `title`, `type_new`, `updated_at`, `created_at`, `description`, `lang_id`) VALUES
(10, '2020-06-01', 1, 1, 1, 'hhhh', 2, '2020-06-01 00:00:00', '2020-06-15 00:00:00', NULL, 0),
(11, '2020-06-01', 1, 1, 1, 'hhhh', 2, '2020-06-24 20:24:29', '2020-06-15 00:00:00', '	description', 1),
(12, '2020-06-01', 1, 1, 1, 'hhhh', 2, '2020-06-01 00:00:00', '2020-06-15 00:00:00', 'Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printe', 0),
(13, '2020-06-01', 1, 1, 1, 'hhhh', 2, '2020-06-01 00:00:00', '2020-06-15 00:00:00', 'Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printe', 0),
(14, '2020-06-24', 1, 2, 1, 'thththththththt,l.;', 1, '2020-06-24 20:25:19', '2020-06-24 20:18:09', NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `symposium`
--

CREATE TABLE `symposium` (
  `id` int(11) NOT NULL,
  `image` varchar(50) NOT NULL,
  `main_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `title` varchar(25) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `vedio` text,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `symposium`
--

INSERT INTO `symposium` (`id`, `image`, `main_id`, `branch_id`, `type_id`, `date`, `title`, `updated_at`, `created_at`, `vedio`, `lang_id`) VALUES
(2, '/uploads/files/Capture.png', 2, 2, 1, '2020-06-10', 'yubhnijmk,l.;', '2020-06-23 21:47:58', '2020-06-20 14:38:06', 'https://www.youtube.com/embed/j9NFSTDFN70', 0),
(3, '/uploads/files/vcredist.bmp', 2, 2, 1, '2020-06-08', 'IT', '2020-06-08 00:00:00', '2019-02-18 14:08:26', 'https://www.youtube.com/embed/j9NFSTDFN70', 0),
(4, '/uploads/files/vcredist.bmp', 2, 2, 1, '2020-06-08', 'IT', '2020-06-08 00:00:00', '2019-02-18 14:08:26', 'https://www.youtube.com/embed/j9NFSTDFN70', 0),
(5, '/uploads/files/vcredist.bmp', 2, 2, 1, '2020-06-08', 'IT', '2020-06-08 00:00:00', '2019-02-18 14:08:26', 'https://www.youtube.com/embed/j9NFSTDFN70', 0),
(6, '/uploads/files/vcredist.bmp', 2, 2, 1, '2020-06-08', 'IT', '2020-06-08 00:00:00', '2019-02-18 14:08:26', 'https://www.youtube.com/embed/j9NFSTDFN70', 0),
(7, '/uploads/files/vcredist.bmp', 2, 2, 1, '2020-06-08', 'IT', '2020-06-08 00:00:00', '2019-02-18 14:08:26', 'https://www.youtube.com/embed/j9NFSTDFN70', 0),
(8, '/uploads/files/Capture.png', 10, 2, 2, '2020-06-10', 'first type', '2020-06-24 19:43:15', '2020-06-24 19:43:04', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `lang_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `name`, `updated_at`, `created_at`, `lang_id`) VALUES
(3, 'Hisham muawen', '2020-06-15 18:34:27', '2020-06-15 18:32:14', 0),
(4, 'هشام معاون', '2020-06-20 15:29:32', '2020-06-20 15:29:32', 0),
(5, 'هشام معاون', '2020-06-24 19:46:14', '2020-06-24 19:46:06', 2);

-- --------------------------------------------------------

--
-- Table structure for table `translator`
--

CREATE TABLE `translator` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `translator`
--

INSERT INTO `translator` (`id`, `name`) VALUES
(1, 'Ahmad'),
(2, 'bassem');

-- --------------------------------------------------------

--
-- Table structure for table `type_event`
--

CREATE TABLE `type_event` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `type_event`
--

INSERT INTO `type_event` (`id`, `name`) VALUES
(1, 'افنراضي'),
(2, 'فيزيائي');

-- --------------------------------------------------------

--
-- Table structure for table `type_lectures`
--

CREATE TABLE `type_lectures` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `type_news`
--

CREATE TABLE `type_news` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `type_news`
--

INSERT INTO `type_news` (`id`, `name`) VALUES
(1, 'دراسات وآراء سياسية'),
(2, 'بحوث ودراسات');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile_country` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_area` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_country` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_area` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastlogin_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `first_name`, `last_name`, `email`, `password`, `mobile_country`, `mobile_area`, `mobile_number`, `phone_country`, `phone_area`, `phone_number`, `status`, `remember_token`, `lastlogin_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'Admin', 'admin@admin.com', '$2y$10$abfrs2VNIwC/9jaOWsXYveZhESdZM4Enbs7D2ugSnR1HbLLo00ZYa', '', '', '', '', '', '', 0, '6STuvu3zZC0C3hFAt10Lq3PEoTiSQaIAa5a34Kzj6I6CxoyfBMnbuZwvvcDR', NULL, NULL, '2020-06-01 09:18:02'),
(2, 1, 'iskandar', 'salama', 'iskandar.salama@gmail.com', '$2y$10$u.HFHlZIBjb7uzpw8b/dy..rcF8Jbld4Asz8LKWYbQWS7GBDv00EG', '5338', '78', '953114', '299', '', '', 0, '1cIwpcffshVzdonUlI9Aghm0Buz3ttaGWvKYb7Kx4ijGHX6SGdrICe2B5Mij', NULL, '2016-11-23 10:24:15', '2020-06-01 09:17:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auther`
--
ALTER TABLE `auther`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lang_id` (`lang_id`),
  ADD KEY `cat_id` (`cat_id`),
  ADD KEY `currency_id` (`currency_id`);

--
-- Indexes for table `book_auther`
--
ALTER TABLE `book_auther`
  ADD PRIMARY KEY (`id`),
  ADD KEY `auther_id` (`auther_id`),
  ADD KEY `book_id` (`book_id`);

--
-- Indexes for table `book_translator`
--
ALTER TABLE `book_translator`
  ADD PRIMARY KEY (`id`),
  ADD KEY `book_id` (`book_id`),
  ADD KEY `translator_id` (`translator_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_cat` (`parent_cat`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries_list`
--
ALTER TABLE `countries_list`
  ADD PRIMARY KEY (`id`),
  ADD KEY `IDX_COUNTRIES_NAME` (`name`),
  ADD KEY `iso2` (`iso2`),
  ADD KEY `iso3` (`iso3`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lectures`
--
ALTER TABLE `lectures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `main_id` (`main_id`),
  ADD KEY `branch_id` (`branch_id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `newsletter`
--
ALTER TABLE `newsletter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `number_count`
--
ALTER TABLE `number_count`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `status` (`status`);

--
-- Indexes for table `panels`
--
ALTER TABLE `panels`
  ADD PRIMARY KEY (`id`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `panels_groups`
--
ALTER TABLE `panels_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order` (`order`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `permissions_slugs`
--
ALTER TABLE `permissions_slugs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permission_id` (`permission_id`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cat_id` (`cat_id`);

--
-- Indexes for table `release`
--
ALTER TABLE `release`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `studies`
--
ALTER TABLE `studies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_new` (`type_new`);

--
-- Indexes for table `symposium`
--
ALTER TABLE `symposium`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type_id` (`type_id`),
  ADD KEY `branch_id` (`branch_id`),
  ADD KEY `symposium_ibfk_2` (`main_id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `translator`
--
ALTER TABLE `translator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_event`
--
ALTER TABLE `type_event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_lectures`
--
ALTER TABLE `type_lectures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_news`
--
ALTER TABLE `type_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `auther`
--
ALTER TABLE `auther`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `book`
--
ALTER TABLE `book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `book_auther`
--
ALTER TABLE `book_auther`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `book_translator`
--
ALTER TABLE `book_translator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `countries_list`
--
ALTER TABLE `countries_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `lectures`
--
ALTER TABLE `lectures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=192;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `newsletter`
--
ALTER TABLE `newsletter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `number_count`
--
ALTER TABLE `number_count`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `panels`
--
ALTER TABLE `panels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `panels_groups`
--
ALTER TABLE `panels_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `permissions_slugs`
--
ALTER TABLE `permissions_slugs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=207;

--
-- AUTO_INCREMENT for table `permission_role`
--
ALTER TABLE `permission_role`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `release`
--
ALTER TABLE `release`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `studies`
--
ALTER TABLE `studies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `symposium`
--
ALTER TABLE `symposium`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `translator`
--
ALTER TABLE `translator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `type_event`
--
ALTER TABLE `type_event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `type_lectures`
--
ALTER TABLE `type_lectures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `type_news`
--
ALTER TABLE `type_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `book`
--
ALTER TABLE `book`
  ADD CONSTRAINT `book_ibfk_1` FOREIGN KEY (`lang_id`) REFERENCES `language` (`id`),
  ADD CONSTRAINT `book_ibfk_2` FOREIGN KEY (`cat_id`) REFERENCES `category` (`id`),
  ADD CONSTRAINT `book_ibfk_3` FOREIGN KEY (`currency_id`) REFERENCES `currency` (`id`);

--
-- Constraints for table `book_auther`
--
ALTER TABLE `book_auther`
  ADD CONSTRAINT `book_auther_ibfk_1` FOREIGN KEY (`auther_id`) REFERENCES `auther` (`id`),
  ADD CONSTRAINT `book_auther_ibfk_2` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`);

--
-- Constraints for table `book_translator`
--
ALTER TABLE `book_translator`
  ADD CONSTRAINT `book_translator_ibfk_1` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`),
  ADD CONSTRAINT `book_translator_ibfk_2` FOREIGN KEY (`translator_id`) REFERENCES `translator` (`id`);

--
-- Constraints for table `gallery`
--
ALTER TABLE `gallery`
  ADD CONSTRAINT `gallery_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`cat_id`) REFERENCES `category` (`id`);

--
-- Constraints for table `studies`
--
ALTER TABLE `studies`
  ADD CONSTRAINT `studies_ibfk_1` FOREIGN KEY (`type_new`) REFERENCES `type_news` (`id`);

--
-- Constraints for table `symposium`
--
ALTER TABLE `symposium`
  ADD CONSTRAINT `symposium_ibfk_1` FOREIGN KEY (`type_id`) REFERENCES `type_event` (`id`),
  ADD CONSTRAINT `symposium_ibfk_2` FOREIGN KEY (`main_id`) REFERENCES `events` (`id`),
  ADD CONSTRAINT `symposium_ibfk_3` FOREIGN KEY (`branch_id`) REFERENCES `department` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
